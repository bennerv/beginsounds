package media_request

import (
	"database/sql"
	"fmt"
	"path/filepath"
	"time"

	"gorm.io/gorm"
)

type MediaRequest struct {
	ID          uint
	URL         string
	MediaType   string
	Name        string
	Filename    string
	StartTime   string
	DeletedAt   *time.Time
	EndTime     string
	Requester   string `gorm:"-"`
	RequesterID sql.NullInt32
	Approved    bool   // 1 byte
	Approver    string `gorm:"-"`
	ApproverID  sql.NullInt32
}

func UploadUnapprovedMedia(db *gorm.DB) {
	// media := Unapproved(db)
	media, _ := ApprovedImages(db)
	// videos := UnapprovedVideos(db)

	// os.Create("tmp/Images")
	// os.Create("tmp/Videos")

	fmt.Printf("len(media) = %+v\n", len(media))

	for _, m := range media {

		e := filepath.Ext(m.URL)
		tx := db.Model(&m).Update("filename", fmt.Sprintf("%s%s", m.Name, e))
		if tx.Error != nil {
			fmt.Printf("Error Updating filename %+v\n", tx.Error)
			continue
		}

		// dstFile := fmt.Sprintf("../../tmp/memes/%s", m.Filename)
		// fmt.Printf("dstFile = %+v\n", dstFile)
		// err := utils.DownloadFile(m.Url, dstFile)

		// if err != nil {
		// 	fmt.Printf("err = %+v\n", err)
		// }

		// src, err := os.Open(
		// 	fmt.Sprintf("/home/begin/stream/Stream/ViewerImages/%s", image.Filename)
		// )
		// if err != nil {
		// 	fmt.Printf("Error Opening src file %+v\n", err)
		// 	continue
		// }
		// defer src.Close()
		// defer f.Close()
		// nBytes, err := io.Copy(f, src)
		// if err != nil {
		// 	fmt.Printf("Error Copying %+v\n", err)
		// }

		// fmt.Printf("nBytes = %+v\n", nBytes)
		//
	}

	//for _, video := range videos {
	//	//
	//}

	// I could copy all images and videos into a tmp fole

	// s3cmd --no-mime-magic --acl-public --delete-after --recursive put \
	// 	/home/begin/stream/Stream/ViewerVideos \
	// 	s3://beginworld/memes/

	// We need to iterate through each of these
	// and upload it to the linode

}

func All(db *gorm.DB) []*MediaRequest {
	var results []*MediaRequest
	db.Find(&results)
	return results
}

func FindUnapprovedByName(db *gorm.DB, name string) (*MediaRequest, error) {
	var result MediaRequest
	tx := db.Model(&MediaRequest{}).
		Where("approved IS false AND deleted_at IS NULL AND name = ?", name).
		Scan(&result)
	return &result, tx.Error
}

func FindByName(db *gorm.DB, name string) (*MediaRequest, error) {
	var result MediaRequest
	tx := db.Model(&MediaRequest{}).
		Where("approved IS true AND deleted_at IS NULL AND name = ?", name).
		Scan(&result)
	return &result, tx.Error
}

// UnapprovedImages returns all Media Request with an media_type of image
// 	and are unapproved
func UnapprovedImages(db *gorm.DB) []*MediaRequest {
	var results []*MediaRequest

	db.Model(&MediaRequest{}).
		Where("media_type = 'image' AND approved IS false AND deleted_at IS NULL").
		Scan(&results)

	return results
}

// UnapprovedImages returns all Media Request with an media_type of video
// 	and are unapproved
func UnapprovedVideos(db *gorm.DB) []*MediaRequest {
	var results []*MediaRequest
	db.Model(&MediaRequest{}).
		Where("media_type = 'video' AND approved IS false AND deleted_at IS NULL").
		Scan(&results)
	return results
}

// ApprovedImages returns all the approved Media Requests
// that have the media_type image
func ApprovedImages(db *gorm.DB) ([]MediaRequest, error) {
	var results []MediaRequest

	tx := db.Model(&MediaRequest{}).
		Where("media_type = 'image' AND approved IS true AND deleted_at IS NULL").
		Scan(&results)

	return results, tx.Error
}

// ApprovedVideos all approved videos with the media type video
func ApprovedVideos(db *gorm.DB) ([]MediaRequest, error) {
	var results []MediaRequest

	tx := db.Model(&MediaRequest{}).
		Where("media_type = 'video' AND approved IS true AND deleted_at IS NULL").
		Scan(&results)

	return results, tx.Error
}

func Images(db *gorm.DB) []string {
	var results []string
	// We want the deleted at not to be null,
	// because that means we successfully processed
	db.Model(&MediaRequest{}).
		Select("name").
		Where("approved = true AND deleted_at IS NOT NULL AND media_type = 'video'").
		Distinct("name").
		Order("name").
		Scan(&results)
	return results
}

func AllMedia(db *gorm.DB) []*MediaRequest {
	var media []*MediaRequest

	db.Model(&MediaRequest{}).Select("*").
		Where("approved = true AND deleted_at IS NOT NULL").
		Distinct("name").Order("name").Scan(&media)

	return media
}

func Videos(db *gorm.DB) []string {
	var results []string
	// We want the deleted at not to be null,
	// because that means we successfully processed
	db.Model(&MediaRequest{}).
		Select("name").
		Where("approved = true AND deleted_at IS NOT NULL AND media_type = 'image'").
		Order("name").
		Distinct("name").
		Scan(&results)
	return results
}

// Unapproved return all unapproved media_requests
func Unapproved(db *gorm.DB) []*MediaRequest {
	var results []*MediaRequest

	db.Model(&MediaRequest{}).
		Where("approved = false AND deleted_at IS NULL").
		Scan(&results)

	return results
}
