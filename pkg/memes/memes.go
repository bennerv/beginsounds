package memes

import (
	"fmt"
	"image"
	"os"

	// Package image/jpeg is not used explicitly in the code below,
	// but is imported for its initialization side-effect, which allows
	// image.Decode to understand JPEG formatted images. Uncomment these
	// two lines to also understand GIF and PNG images:
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Meme struct {
	ID           uint
	Name         string
	X            float64
	Y            float64
	Scale        float64
	Rotation     float64
	PositionType string
	Enabled      bool
	Width        int
	Height       int
	MemeType     string
	Filename     string
}

func ExtractFileinfo(filename string) (int, int, string) {
	var width int
	var height int
	var memeType string

	f, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Error Opening Meme: %+v\n", err)
		return width, height, memeType
	}

	im, memeType, _ := image.DecodeConfig(f)
	width = im.Width
	height = im.Height

	return width, height, memeType
}

func convertToFloat64(key string, settings map[string]interface{}) float64 {
	rawVal, _ := settings[key]
	return rawVal.(float64)
}

// TODO: Add this later
// Width        int
// Height       int
// MemeType     string
// Filename     string
func CreateCurrentMeme(db *gorm.DB, name string, settings map[string]interface{}) error {
	meme := Meme{
		Name:         name,
		Y:            convertToFloat64("y", settings),
		X:            convertToFloat64("x", settings),
		Scale:        convertToFloat64("scale", settings),
		Rotation:     convertToFloat64("rotation", settings),
		PositionType: "current",
		MemeType:     "image", // THIS IS A HACK
	}

	tx := db.Create(&meme)
	return tx.Error
}

func (m *Meme) SaveSettings(db *gorm.DB, settings map[string]interface{}) error {
	meme := Meme{
		Y:        convertToFloat64("y", settings),
		X:        convertToFloat64("x", settings),
		Scale:    convertToFloat64("scale", settings),
		Rotation: convertToFloat64("rotation", settings),
	}

	tx := db.Model(&m).Where("ID = ?", m.ID).Updates(meme)
	return tx.Error
}

func (m *Meme) Settings() map[string]interface{} {
	settings := map[string]interface{}{
		"x":        m.X,
		"y":        m.Y,
		"scale":    m.Scale,
		"rotation": m.Rotation,
	}
	return settings
}

func (m *Meme) Enable(db *gorm.DB) error {
	tx := db.Model(m).Update("enabled", true)
	return tx.Error
}

func (m *Meme) Disable(db *gorm.DB) error {
	tx := db.Model(m).Update("enabled", false)
	return tx.Error
}

func FindAllByName(db *gorm.DB, name string) ([]*Meme, error) {
	var res []*Meme
	tx := db.Where("name = ?", name).Find(&res)
	return res, tx.Error
}

func All(db *gorm.DB) ([]*Meme, error) {
	var res []*Meme
	tx := db.Find(&res)
	return res, tx.Error
}

func AllDefaults(db *gorm.DB) ([]*Meme, error) {
	var res []*Meme
	tx := db.Where("position_type = 'default'").Find(&res)
	return res, tx.Error
}

func FindCurrent(db *gorm.DB, name string) (*Meme, error) {
	var m Meme
	tx := db.Where("name = ? AND position_type = 'current'", name).First(&m)
	return &m, tx.Error
}

func FindDefault(db *gorm.DB, name string) (*Meme, error) {
	var m Meme
	tx := db.Where("name = ? AND position_type = 'default'", name).First(&m)
	return &m, tx.Error
}

func Find(db *gorm.DB, name string) (*Meme, error) {
	var m Meme
	tx := db.Where("name = ?", name).First(&m)
	return &m, tx.Error
}

func (m *Meme) Save(db *gorm.DB) error {
	tx := db.Clauses(clause.OnConflict{
		Columns: []clause.Column{{Name: "name"}, {Name: "position_type"}},
		DoUpdates: clause.AssignmentColumns([]string{
			"x", "y", "scale", "rotation"}),
	}).Create(&m)
	return tx.Error
}
