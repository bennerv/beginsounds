package memes

import (
	"fmt"
	"path/filepath"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
)

func TestExtractingFile(t *testing.T) {
	filename, err := filepath.Abs("../../test_data/bmo.gif")
	if err != nil {
		t.Error("Error opening gif for test")
	}
	w, _, _ := ExtractFileinfo(filename)
	if w != 250 {
		t.Errorf("We aren't getting width back incorrectly: %d", w)
	}
}

func TestSavingMeme(t *testing.T) {
	// Lets iterate through all the memes, based on unique names

	db := database.CreateDBConn("test_beginsounds3")
	var names []string
	db.Raw("SELECT name FROM memes GROUP BY name").Scan(&names)

	for _, name := range names {
		memes, _ := FindAllByName(db, name)

		var fp *Meme
		var np *Meme
		for _, meme := range memes {
			if meme.PositionType == "" {
				np = meme
			} else {
				fp = meme
			}

			if np != nil {
				if fp == nil {
					err := db.Model(&np).Update("position_type", "default")
					if err != nil {
						fmt.Printf("err = %+v\n", err)
					}
				} else {
					if fp.PositionType == "current" {
						err := db.Model(&np).Update("position_type", "default")
						if err != nil {
							fmt.Printf("err = %+v\n", err)
						}
					} else {
						err := db.Model(&np).Update("position_type", "current")
						if err != nil {
							fmt.Printf("err = %+v\n", err)
						}
					}
				}
			}
		}
		// we need to check which is empty and what to other is

		// I need to then figure out what to assign
		// each meme that doesn't have a position
	}

	// Lets iterate through every Meme
}
