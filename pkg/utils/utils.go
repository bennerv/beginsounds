package utils

import (
	"context"
	"errors"
	"io"
	"net/http"
	"os"
	"regexp"
	"sync"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

func ChatFanIn(ctx context.Context, channels ...<-chan chat.ChatMessage) <-chan chat.ChatMessage {
	multiplexedStream := make(chan chat.ChatMessage)
	var wg sync.WaitGroup

	// I am not returning of using this function
	multiplex := func(c <-chan chat.ChatMessage) {
		defer wg.Done()

		for i := range c {
			select {
			case <-ctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))

	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		wg.Wait()
		defer close(multiplexedStream)
	}()

	return multiplexedStream
}

func StringFanIn(ctx context.Context, channels ...<-chan string) <-chan string {
	multiplexedStream := make(chan string)
	var wg sync.WaitGroup

	// I am not returning of using this function
	multiplex := func(c <-chan string) {
		defer wg.Done()

		for i := range c {
			select {
			case <-ctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))

	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		wg.Wait()
		defer close(multiplexedStream)
	}()

	return multiplexedStream
}

// Fanning in is generic
// But this takes a specific type
// audio_request.AudioRequest
func AudioRequestFanIn(
	ctx context.Context,
	channels ...<-chan audio_request.AudioRequest,
) <-chan audio_request.AudioRequest {

	multiplexedStream := make(chan audio_request.AudioRequest)
	var wg sync.WaitGroup

	// I am not returning of using this function
	multiplex := func(c <-chan audio_request.AudioRequest) {
		defer wg.Done()

		for i := range c {
			select {
			case <-ctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))

	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		wg.Wait()
		defer close(multiplexedStream)
	}()

	return multiplexedStream
}

func IsTimeStamp(ts string) bool {
	var validTS = regexp.MustCompile(`^[0-9]+:[0-9]+(.[0-9]+)?$`)
	return validTS.MatchString(ts)
}

func DownloadFile(url string, filename string) error {
	// nazarchops: tenor works with curl if you set Accept header to image/webp
	//Get the response bytes from the url
	// response, err := client.Get(url)

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Accept", "image/webp")

	response, err := client.Do(req)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return errors.New("Received non 200 response code")
	}
	//Create a empty file
	// Where do we want to save these
	// Probably the similar to where we save our audio folder
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	//Write the bytes to the file
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil
}

// TODO: potentially in the future
// 			 match on multiple layouts
func DurationExtractor(t string) float64 {
	// layout := "15:04:05"
	layout := "04:05"
	startTime, _ := time.Parse(layout, "00:00")
	parsedTime, _ := time.Parse(layout, t)
	return time.Duration(parsedTime.UnixNano() - startTime.UnixNano()).Seconds()
}
