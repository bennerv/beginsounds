package streamgod_router

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/party"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func Route(
	ctx context.Context,
	db *gorm.DB,
	dbChan chat.DBChan,
) (
	<-chan string,
	<-chan string,
	<-chan string,
	<-chan audio_request.AudioRequest,
	<-chan bool,
	<-chan chat.ChatMessage,
) {

	chatResults := make(chan string, 1000)
	botbotResults := make(chan string, 1000)
	websocketResults := make(chan string, 1000)
	aqs := make(chan audio_request.AudioRequest)
	noMemes := make(chan bool, 1)
	obsRequests := make(chan chat.ChatMessage)

	go func() {
		defer close(websocketResults)
		defer close(chatResults)
		defer close(botbotResults)
		defer close(aqs)
		defer close(noMemes)
		defer close(obsRequests)

	MsgLoop:
		for msg := range dbChan.Messages {
			rand.Seed(time.Now().UnixNano())

			select {
			case <-ctx.Done():
				return
			default:
				if !msg.Streamgod {
					continue MsgLoop
				}

				parsedCmd := msg.ParsedCmd
				parts := msg.Parts
				targetAmount := parsedCmd.TargetAmount
				if targetAmount < 1 {
					targetAmount = 1
				}

				switch parsedCmd.Name {

				case "enable":
					if parsedCmd.TargetCommandID != 0 {
						sc := stream_command.FindByID(db, parsedCmd.TargetCommandID)
						sc.Enable(db)
						chatResults <- fmt.Sprintf("Sound Enabled: %s", sc.Name)
					}

					if len(parts) > 1 {
						potentialMeme := parts[1]
						m, _ := memes.Find(db, potentialMeme)
						if m.ID != 0 {
							m.Enable(db)
							chatResults <- fmt.Sprintf("Meme Enabled: @%s", m.Name)
						}
					}

					// We now need to check if theres potentially a meme
				case "disable":
					fmt.Println("Attempting to Disable")

					if parsedCmd.TargetCommandID != 0 {
						sc := stream_command.FindByID(db, parsedCmd.TargetCommandID)
						err := sc.Disable(db)
						if err != nil {
							fmt.Printf("Error disabling Command: %+v\n", err)
						}
						chatResults <- fmt.Sprintf("Sound Disabled: !%s", sc.Name)
					}

					fmt.Printf("parts = %+v\n", parts)

					if len(parts) > 1 {
						potentialMeme := parts[1]
						m, _ := memes.FindDefault(db, potentialMeme)
						if m.ID != 0 {
							m.Disable(db)
							chatResults <- fmt.Sprintf("Meme Disabled: @%s", m.Name)
						}
					}

				case "cage", "jail":
					if parsedCmd.TargetUserID != 0 {
						tu := player.FindByID(db, parsedCmd.TargetUserID)
						chatResults <- fmt.Sprintf("Get in yo Cage: @%s", tu.Name)
						tu.SendToJail(db)
					}
				case "release":
					if parsedCmd.TargetUserID != 0 {
						tu := player.FindByID(db, parsedCmd.TargetUserID)
						chatResults <- fmt.Sprintf("@%s has been Freed!", tu.Name)
						tu.ReleaseFromJail(db)
					}
				case "makegod":
					if parsedCmd.TargetUserID != 0 {
						tu := player.FindByID(db, parsedCmd.TargetUserID)
						chatResults <- fmt.Sprintf("@%s is Now a Stream God!", tu.Name)
						tu.MakeGod(db)
					}
				case "removegod":
					if parsedCmd.TargetUserID != 0 {
						tu := player.FindByID(db, parsedCmd.TargetUserID)
						chatResults <- fmt.Sprintf("@%s is NO LONGER a Stream God!", tu.Name)
						tu.RemoveGod(db)
					}
				case "makelord":
					if parsedCmd.TargetUserID != 0 {
						tu := player.FindByID(db, parsedCmd.TargetUserID)
						chatResults <- fmt.Sprintf("@%s is Now a Stream Lord!", tu.Name)
						tu.MakeLord(db)
					}
				case "removelord":
					if parsedCmd.TargetUserID != 0 {
						tu := player.FindByID(db, parsedCmd.TargetUserID)
						chatResults <- fmt.Sprintf("@%s is NO LONGER a Stream Lord!", tu.Name)
						tu.RemoveLord(db)
					}
				case "mosaic":
					buildMosaic(db, parts)
				case "passthejester":
					PassTheJester(dbChan.DB, msg, &parsedCmd, chatResults, aqs)
				case "unapproveparty":
					tx := dbChan.DB.Where("id = ?", parsedCmd.TargetAmount).Delete(&party.Party{})
					if tx.Error != nil {
						fmt.Printf("tx.Error = %#v\n", tx.Error)
					}
					botbotResults <- fmt.Sprintf("Party ID: %d Unapproved", parsedCmd.TargetAmount)
					continue MsgLoop
				case "approveparty":
					dbChan.DB.Model(&party.Party{}).
						Where("id = ?", parsedCmd.TargetAmount).
						Update("approved", true)
					botbotResults <- fmt.Sprintf("Party ID: %d Approved", parsedCmd.TargetAmount)
					continue MsgLoop
				case "unapprovedparties":
					var res []*party.Party
					dbChan.DB.Where("approved = false").Find(&res)
					for _, p := range res {
						botbotResults <- fmt.Sprintf("%d: %s - %s", p.ID, p.Name, p.Manifesto)
					}
				case "nomeme":
					fmt.Println("WE WANT NO MEMESSSSSS!!!!!!!!")
					noMemes <- true
				case "artleaker":
					fmt.Println("\nART MATT IS LEAKING CREDS")

					sj := stream_jester.NewSecret(dbChan.DB)
					fmt.Printf("sj = %+v\n", sj)
					continue MsgLoop
				case "chaos":
					fmt.Println("Attempting to Flip Chaos")
					chaosMode, _ := stream_jester.ChaosMode(db, stream_jester.SyncWTF)

					if chaosMode {
						obsRequests <- chat.ChatMessage{
							Message:    "!zoombegin2",
							Streamgod:  true,
							PlayerID:   msg.PlayerID,
							PlayerName: msg.PlayerName,
						}
						chatResults <- "CHAOS MODE!!!!!"
						continue MsgLoop
					}

					chatResults <- "The Chaos MUST BE REIGNED IN."
				case "dropeffect":
					dropeffect(
						dbChan.DB,
						&parsedCmd,
						targetAmount,
						chatResults,
						botbotResults,
						websocketResults,
					)
				}
			}
		}
	}()

	return botbotResults, websocketResults, chatResults, aqs, noMemes, obsRequests
}
