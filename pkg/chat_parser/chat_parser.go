package chat_parser

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

func Parse(
	db *gorm.DB,
	p *player.Player,
	msg string,
) (chat.ChatMessage, error) {

	parts := strings.Split(msg, " ")
	parsedCmd := chat.ParsedCommand{
		Username:   p.Name,
		Streamgod:  p.Streamgod,
		Streamlord: p.Streamlord,
	}
	cm := chat.ChatMessage{
		PlayerID:   p.ID,
		PlayerName: p.Name,
		Streamlord: p.Streamlord,
		Streamgod:  p.Streamgod,
		StreetCred: p.StreetCred,
		CoolPoints: p.CoolPoints,
		Message:    msg,
		Parts:      parts,
	}

	name := string(parts[0])
	fc := string(name[0])
	if fc == "@" || fc == "!" {
		name = name[1:]
	}
	parsedCmd.Name = strings.ToLower(name)

	for _, part := range parts[1:] {
		if len(part) == 0 {
			return cm, errors.New("Not parts after stripping message")
		}

		fc := string(part[0])
		if fc == "@" || fc == "!" {
			part = part[1:]
		}
		part = strings.ToLower(part)

		tp, _ := pack.FindByName(db, part)
		if tp.ID != 0 {
			parsedCmd.TargetPackName = tp.Name
		}

		c := stream_command.FindNoTheme(db, part)
		if c.ID != 0 {
			parsedCmd.TargetCommand = c.Name
			parsedCmd.TargetCommandID = c.ID
		}

		// This find by name is not just query unappproved
		// m, _ := media_request.FindByName(db, part)
		m, _ := media_request.FindUnapprovedByName(db, part)
		if m.ID != 0 {
			parsedCmd.TargetMedia = m.Name
		}

		// We are checking the Street of the TargetUser
		altN := "송써니"
		if part == altN {
			part = "ssyuni"
		}
		p := player.Find(db, part)
		if p.ID != 0 {
			parsedCmd.TargetUser = p.Name
			parsedCmd.TargetUserID = p.ID
		}

		// We should make sure intV is always positive
		intV, err := strconv.Atoi(part)
		if err == nil {
			if intV < 0 {
				return cm, errors.New("You can't use negative amounts")
			}
			parsedCmd.TargetAmount = intV
		}
	}

	cm.ParsedCmd = parsedCmd
	return cm, nil
}
