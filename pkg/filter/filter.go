package filter

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/pokemon"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

// I don't like how the chat.ChatMessage, is passed
// still as a ChatMessage, when its a command now
// Maybe we should move it to a different type
func RouteUserCommands(ctx context.Context, db *gorm.DB, messages <-chan chat.ChatMessage) (
	<-chan chat.ChatMessage,
	<-chan string) {

	commands := make(chan chat.ChatMessage, 10)
	results := make(chan string, 10)

	usersWeHate := []string{"nightbot"}

	go func() {
		defer close(commands)
		defer close(results)

	outside:
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
				if parser.IsCommand(msg.Message) {
					for _, u := range usersWeHate {
						if u == msg.PlayerName {
							fmt.Println("Bad User: ", u)
							continue outside
						}
					}

					commands <- msg
				}
			}
		}

	}()

	return commands, results
}

func AudioRequests(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (
	<-chan audio_request.AudioRequest,
	<-chan string,
	<-chan chat.ChatMessage,
) {

	sfxs := make(chan audio_request.AudioRequest, 10)
	results := make(chan string, 10)
	obsRequests := make(chan chat.ChatMessage, 10)

	go func() {
		defer close(sfxs)
		defer close(results)
		defer close(obsRequests)

	Loop:
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
				aq, err := soundboard.CreateAudioRequestFromChatMsg(db, &msg)

				if err != nil {
					results <- fmt.Sprintf("%v", err)
				}

				if aq.Name != "" {
					if aq.Name == "wideputin" {
						obsRequests <- chat.ChatMessage{
							Message: "!widebegin",
						}
					}

					sc := stream_command.Find(db, aq.Name)
					if !sc.Enabled {
						results <- fmt.Sprintf("!%s is disabled", aq.Name)
						continue Loop
					}

					sfxs <- *aq
					continue Loop
				}

				if err != nil {
					continue Loop
				}

			}
		}

	}()

	return sfxs, results, obsRequests
}

// TODO: Move out the Pokemon Guessing
func RoutePokemonCommands(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (

	<-chan audio_request.AudioRequest,
	<-chan string) {

	sfxs := make(chan audio_request.AudioRequest, 10)
	results := make(chan string, 10)

	go func() {
		defer close(sfxs)
		defer close(results)

	Loop:
		for msg := range messages {
			select {
			case <-ctx.Done():
				return
			default:
				var cmd string

				// What the heck is this!?!?!
				if len(msg.Message) > 6 {
					cmd = msg.Message[0:6]
				}

				if cmd == "!guess" {
					guess, correct, guessCount := pokemon.CheckGuess(db, msg)

					if correct {
						victoryMsg := fmt.Sprintf("%s Won! Guess: %s | Total Guesses: %d",
							msg.PlayerName, guess, guessCount)

						audioRequest, err := soundboard.CreateAudioRequest(
							db, "pokewin", "beginbot")

						if err != nil {
							fmt.Printf("Error Creating Audio Request for Pokemon Winner: %+v\n", err)
							continue Loop
						}

						sfxs <- *audioRequest
						results <- victoryMsg
					} else {
						fmt.Printf("\n\t%s Not correct s Guess: %s", msg.PlayerName, guess)
					}

					if !correct {
						loserMsg := fmt.Sprintf("@%s guessed wrong!", msg.PlayerName)
						results <- loserMsg
					}

				}

				if msg.Message == "!pokemon" {
					answer := pokemon.CurrentAnswer(db)
					audioRequest := soundboard.CreateAdminAudioRequest(db, answer.Pokemon)
					sfxs <- audioRequest
				}
			}
		}

	}()

	return sfxs, results
}
