package config

import (
	"fmt"
	"net"
	"os"
	"time"
)

type Config struct {
	Channel  string
	Nick     string
	Conn     net.Conn
	Cooldown time.Duration
}

func findCoolDown() time.Duration {
	const DEFAULT_COOLDOWN_PERIOD = "60s"

	rawCoolDown := getenv("COOLDOWN_PERIOD", DEFAULT_COOLDOWN_PERIOD)
	cooldown, err := time.ParseDuration(rawCoolDown)

	if err != nil {
		errorMsg := fmt.Errorf("Invalid COOLDOWN_PERIOD %s. Error: %s", rawCoolDown, err)
		fmt.Println(errorMsg)
		cooldown, _ = time.ParseDuration(DEFAULT_COOLDOWN_PERIOD)
	}

	return cooldown
}

func getenv(name string, defaultValue string) string {
	v, ok := os.LookupEnv(name)
	if !ok {
		return defaultValue
	}
	return v
}

func connCreator(channel string, nickname string, oauthToken string) net.Conn {
	conn, err := net.Dial("tcp", "irc.chat.twitch.tv:6667")

	if err != nil {
		panic(fmt.Errorf("We got an error: %s", err))
	}

	fmt.Fprintf(conn, "PASS %s\r\n", oauthToken)
	fmt.Fprintf(conn, "NICK %s\r\n", nickname)
	fmt.Fprintf(conn, "JOIN #%s\r\n", channel)

	return conn
}

func oauthFinder() string {
	tokenKey := "BEGINBOT_TWITCH_OAUTH_TOKEN"
	oauthToken := os.Getenv(tokenKey)
	if oauthToken == "" {
		panic(fmt.Sprintf(`You must set the %s Environment Variable.
		You can find one here: https://twitchapps.com/tmi/`, tokenKey))
	}
	return oauthToken
}

func NewIrcConfig(channel string) Config {
	// channel := getenv("TWITCH_CHANNEL", "beginbot")

	nickname := getenv("BEGINBOT_TWITCH_BOTNAME", "beginbotbot")
	oauthToken := oauthFinder()

	cooldown := findCoolDown()
	conn := connCreator(channel, nickname, oauthToken)

	return Config{
		Channel:  channel,
		Nick:     nickname,
		Conn:     conn,
		Cooldown: cooldown}
}
