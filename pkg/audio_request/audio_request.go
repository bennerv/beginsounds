package audio_request

import (
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

// Should we save the StreamCommandID on the AudioRequest
type AudioRequest struct {
	ID           int
	Filename     string
	Name         string
	Path         string
	Played       bool
	Streamlord   bool
	StreamJester bool
	Notify       bool
	Free         bool
	CreatedAt    time.Time

	Player   *player.Player `gorm:"foreignKey:PlayerID"`
	PlayerID int
}

type PlayCount struct {
	Name  string
	Total int
}

func (a AudioRequest) String() string {
	return fmt.Sprintf("AudioRequest<%d %s %d Streamlord: %t>",
		a.ID, a.Name, a.PlayerID, a.Streamlord)
}

func Save(db *gorm.DB, m *AudioRequest) {
	db = db.Create(&m)
	if db.Error != nil {
		fmt.Println("Err Saving Audio Request: ", db.Error)
	}
}

func MostPopularSounds(db *gorm.DB) []PlayCount {
	start := time.Now()
	yesterday := start.Add(time.Hour * -20)

	var res []PlayCount

	db = db.Table("audio_requests").
		Raw(`SELECT name, count(*) as total FROM audio_requests
		WHERE created_at > ?
		GROUP BY name
		ORDER BY total DESC`, yesterday).Scan(&res)

	if db.Error != nil {
		fmt.Println("Error finding the most popular AudioRequests: ", db.Error)
	}

	return res
}

// TODO: This is really total sounds player Today
func TotalPlayedSounds(db *gorm.DB) int64 {
	start := time.Now()
	yesterday := start.Add(time.Hour * -20)
	var count int64
	db = db.Table("audio_requests").Where("created_at > ?", yesterday).Count(&count)
	if db.Error != nil {
		fmt.Printf("No Audio Requests: %+v", db.Error)
		return 0
	}
	return count
}

func Count(db *gorm.DB) int64 {
	var count int64
	db.Table("audio_requests").Count(&count)
	return count
}
