package audio_request

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
	"gorm.io/gorm"
)

func TestMostPopularSounds(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	aq := buildAQ(db, "beginbot", "hello", true)
	Save(db, &aq)

	p := player.Find(db, "beginbot")

	res := MostPopularSounds(db)
	if len(res) != 1 {
		t.Error("WE did not get any sounds!")
		return
	}

	if res[0].Name != "hello" {
		t.Error("WE did not get the most popular sound")
	}

	start := time.Now()
	yesterday := start.Add(time.Hour * -24)
	a := AudioRequest{
		Filename: "imback.ogg",
		Name:     "imback",
		Path:     "/home/begin/stream/Stream/Samples/imback.opus",
		PlayerID: p.ID,
		// Username:   "beginbot",
		Streamlord: true,
		Played:     true,
		CreatedAt:  yesterday,
	}
	Save(db, &a)
	res = MostPopularSounds(db)
	fmt.Printf("res = %+v\n", res)
	if len(res) != 1 {
		t.Errorf("We should only have one Sound %d", len(res))
		return
	}
	if res[0].Name != "hello" {
		t.Errorf("WE did not get the most popular sound: %s", res[0].Name)
	}
	if res[0].Total != 1 {
		t.Errorf("WE did not get the most popular sound count correct: %d", res[0].Total)
	}
}

func TestTotalPlayedSounds(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	total := TotalPlayedSounds(db)
	if total != 0 {
		t.Errorf("We should have not a single Audio Request: %d", total)
	}

	p := player.CreatePlayerFromName(db, "beginbot")
	a := AudioRequest{
		Filename:   "imback.ogg",
		Name:       "imback",
		Path:       "/home/begin/stream/Stream/Samples/imback.opus",
		PlayerID:   p.ID,
		Streamlord: true,
	}
	Save(db, &a)
	total = TotalPlayedSounds(db)
	if total != 1 {
		t.Error("We should have a single Audio Request")
	}

	start := time.Now()
	yesterday := start.Add(time.Hour * -24)
	a = AudioRequest{
		Filename:   "imback.ogg",
		Name:       "imback",
		Path:       "/home/begin/stream/Stream/Samples/imback.opus",
		PlayerID:   p.ID,
		Streamlord: true,
		CreatedAt:  yesterday,
	}
	Save(db, &a)
	total = TotalPlayedSounds(db)
	if total != 1 {
		t.Error("We should only pull Audio Requests for the last 24 hours")
	}
}

func TestCount(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	count := Count(db)

	if count != 0 {
		t.Error("We should have 0 Audio requests")
	}
	aq := buildAQ(db, "beginbot", "hello", true)
	Save(db, &aq)
	count = Count(db)
}

// This is really test Setup
func buildAQ(db *gorm.DB, username string, sfx string, played bool) AudioRequest {
	p := player.CreatePlayerFromName(db, "beginbot")

	return AudioRequest{
		Filename:   fmt.Sprintf("%s.ogg", sfx),
		Name:       sfx,
		Path:       fmt.Sprintf("/home/begin/stream/Stream/Samples/%s.ogg", sfx),
		PlayerID:   p.ID,
		Played:     played,
		Streamlord: true,
	}
}
