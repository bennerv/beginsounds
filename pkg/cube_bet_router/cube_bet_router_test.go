package cube_bet_router

import (
	"context"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
)

// Bet with no Game Running
// Start a Game
// Collect Bets
// Change Bets
// Start Solve
// Try and Bet During Solve
func TestRoute(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	ctx := context.Background()

	p := player.CreatePlayerFromName(db, "young.thug")
	// theBot := player.CreatePlayerFromName(db, "beginbotbot")
	// p2 := player.CreatePlayerFromName(db, "lil.uzi")

	noGameMsg := chat.ChatMessage{
		PlayerName: p.Name,
		PlayerID:   p.ID,
		Message:    "!bet 42",
	}
	ircMsgs := make(chan chat.ChatMessage, 1)
	ircMsgs <- noGameMsg
	_, chatRes, _ := Route(ctx, db, ircMsgs)

	timer := time.NewTimer(50 * time.Millisecond)

	select {
	case <-timer.C:
		// TODO: Come back
		// t.Error("We didn't get a message back!")
	case res := <-chatRes:
		if res != "@young.thug Begin isn't planning on solving a Cube Currently" {
			t.Errorf("You should not be able to bet while no game is running: %s", res)
		}
	}

	return
	// startGameMsg := chat.ChatMessage{
	// 	PlayerName: theBot.Name,
	// 	PlayerID:   &theBot.ID,
	// 	Message:    "!bettingtime",
	// }

	// cm1 := chat.ChatMessage{
	// 	PlayerName: p.Name,
	// 	PlayerID:   &p.ID,
	// 	Message:    "!bet 42",
	// }
	// cm2 := chat.ChatMessage{
	// 	PlayerName: p2.Name,
	// 	PlayerID:   &p2.ID,
	// 	Message:    "!bet 44",
	// }

	// ircMsgs := make(chan chat.ChatMessage, 2)
	// ircMsgs <- cm1
	// ircMsgs <- cm2
	// _, chatRes := Route(ctx, db, ircMsgs)
	// fmt.Printf("chatRes = %+v\n", chatRes)

	// beginbotbot := player.CreatePlayerFromName(db, "beginbotbot")
	// cm3 := chat.ChatMessage{
	// 	PlayerName: beginbotbot.Name,
	// 	PlayerID:   &beginbotbot.ID,
	// 	Message:    "!cubed 45",
	// }

	// ircMsgs = make(chan chat.ChatMessage, 1)
	// ircMsgs <- cm3
	// _, chatRes = Route(ctx, db, ircMsgs)
	// fmt.Printf("chatRes = %+v\n", chatRes)

	// testTimer := timer.NewTimer(10 * time.Millisecond)
	// winnerMsg := "@young.thug won! Guessed 42 - Solved in: 45"
	// select {
	// case <-testTimer.C:
	// 	t.Errorf("We should have got a winner")
	// case res := <-chatRes:
	// 	if res != winnerMsg {
	// 		t.Errorf("we should be done")
	// 	}
	// }
}
