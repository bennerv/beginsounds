package pack

import (
	"errors"
	"fmt"

	"github.com/lib/pq"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

type Pack struct {
	ID   int
	Name string
}

type PacksPlayers struct {
	PlayerID int
	PackID   int
}

type CommandsPacks struct {
	CommandID int
	PackID    int
}

type PackResult struct {
	Pack
	Players  pq.StringArray `gorm:"type:text[]"`
	Commands pq.StringArray `gorm:"type:text[]"`
}

type CommandRes struct {
	ID   int
	Name string
	Cost int
}

func Give(db *gorm.DB, packID int, playerID int) (*PacksPlayers, error) {
	pp := PacksPlayers{
		PlayerID: playerID,
		PackID:   packID,
	}

	tx := db.Create(&pp)
	if tx.Error != nil {
		fmt.Printf("Error giving user pack: %+v\n", tx.Error)
	}

	return &pp, tx.Error
}

func (p *Pack) Commands(db *gorm.DB) []string {
	var results []string
	db.Raw(`
		SELECT
			c.name
		FROM
			packs p
		INNER JOIN
			commands_packs cp
		ON
			p.id = cp.pack_id
		INNER JOIN
			stream_commands c
		ON
			cp.command_id = c.id
		WHERE
			p.id = ?
	`, p.ID).Scan(&results)
	return results
}

func Find(db *gorm.DB, name string) (*Pack, error) {
	var p Pack
	db.Model(&p).Where("name = ?", name).Scan(&p)
	return &p, nil
}

// AllCommandsForPlayer returns all the owned sounds
// 	from the Player's packs
func AllCommandsForPlayer(
	db *gorm.DB,
	playerName string,
) []stream_command.StreamCommand {
	var results []stream_command.StreamCommand
	db.Raw(`
		SELECT
			c.id, c.name, c.cost
		FROM
			packs p
		INNER JOIN
			commands_packs cp
		ON
			p.id = cp.pack_id
		INNER JOIN
			stream_commands c
		ON
			cp.command_id = c.id
		WHERE
			p.id = ?
	`, playerName).Scan(&results)
	return results
}

func CommandsInPack(
	db *gorm.DB,
	id int,
) []stream_command.StreamCommand {
	var results []stream_command.StreamCommand
	db.Raw(`
		SELECT
			c.id, c.name, c.cost
		FROM
			packs p
		INNER JOIN
			commands_packs cp
		ON
			p.id = cp.pack_id
		INNER JOIN
			stream_commands c
		ON
			cp.command_id = c.id
		WHERE
			p.id = ?
	`, id).Scan(&results)
	return results
}

func AllNames(db *gorm.DB) ([]string, error) {
	var res []string
	tx := db.Raw(` SELECT name FROM packs `).Scan(&res)
	return res, tx.Error
}

func All(db *gorm.DB) ([]PackResult, error) {
	var res []PackResult
	tx := db.Raw(`
		SELECT
			packs.id,
			packs.name,
			array_agg(c.name) as commands
		FROM
			packs
		LEFT JOIN
			packs_players pp
		ON
			packs.id = pp.pack_id
		LEFT JOIN
			players
		ON
			pp.player_id = players.id
    LEFT JOIN
      commands_packs cp
    ON
      packs.id = cp.pack_id
    LEFT JOIN
      stream_commands c
    ON
      cp.command_id = c.id
		GROUP BY
			packs.id, packs.name
	`).Scan(&res)
	return res, tx.Error
}

type PackRes struct {
	Name string
}

func EffectForPackSound(db *gorm.DB, commandName string) string {
	var res PackRes
	db.Raw(`
		SELECT
			p.name as name
		FROM
			stream_commands sc
		INNER JOIN
			commands_packs cp
		ON
			sc.id = cp.command_id
		INNER JOIN
			packs p
		ON
			cp.pack_id = p.id
		WHERE
			sc.name = ?
	`, commandName).Scan(&res)
	return res.Name
}

func (p *Pack) AddCommandToPack(db *gorm.DB, commandID int) (*CommandsPacks, error) {
	cp := CommandsPacks{
		CommandID: commandID,
		PackID:    p.ID,
	}
	tx := db.Create(&cp)
	return &cp, tx.Error
}

func (p *Pack) GiveToPlayer(db *gorm.DB, playerName string) (*PacksPlayers, error) {
	playa := player.Find(db, playerName)
	pp := PacksPlayers{
		PlayerID: playa.ID,
		PackID:   p.ID,
	}
	tx := db.Create(&pp)
	return &pp, tx.Error
}

func (p *Pack) Players(db *gorm.DB) []player.Player {
	var players []player.Player

	db.Raw(`
		SELECT
			p.*
		FROM
			players p
		INNER JOIN
			packs_players pp
		ON
			p.ID = pp.player_id
		INNER JOIN
			packs pa
		ON
			pp.pack_id = pa.id
		WHERE
			pa.id = ?`, p.ID).Scan(&players)

	return players
}

func FindByName(db *gorm.DB, name string) (*Pack, error) {
	var p Pack
	tx := db.Where("name = ?", name).Find(&p)
	return &p, tx.Error
}

func Create(db *gorm.DB, name string) (*Pack, error) {
	p := Pack{Name: name}
	tx := db.Create(&p)
	return &p, tx.Error
}

func Count(db *gorm.DB) int64 {
	var count int64
	db.Model(&Pack{}).Count(&count)
	return count
}

func CreatePackFromParts(db *gorm.DB, parts []string) (*Pack, error) {
	var p Pack

	switch len(parts) {
	case 1:
		p.Name = parts[0]
	}

	tx := db.Create(&p)
	fmt.Printf("\tparts = %+v\n", parts)
	return &p, tx.Error
}

func AddSoundsToPacks(
	db *gorm.DB,
	partyName string,
	parts []string,
) ([]string, error) {

	var res []string
	p, _ := FindByName(db, partyName)
	if p.ID == 0 {
		return res, errors.New(fmt.Sprintf("Did not find party: %s", partyName))
	}

	for _, part := range parts {
		sc := stream_command.Find(db, part)
		if sc.ID != 0 {
			_, err := p.AddCommandToPack(db, sc.ID)
			if err == nil {
				res = append(res, part)
			}
		}
	}

	return res, nil
}
