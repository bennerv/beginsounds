package permissions

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestGiveUsernameCommand(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p := player.CreatePlayerFromName(db, "young.thug")
	p2 := player.CreatePlayerFromName(db, "gunna")
	sc := stream_command.CreateFromName(db, "damn")
	Allow(db, p.ID, sc.ID)

	err := Allow(db, p2.ID, sc.ID)

	if err != nil {
		t.Error(err)
	}
	// We need to check gunna has the command
	gunnaOwnsDamn := IsAllowed(db, p2.ID, sc.ID)
	if !gunnaOwnsDamn {
		t.Error("Why doesn't gunna own !damn")
	}
}

// this should be a question
func TestRemoveUserAccess(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p := player.CreatePlayerFromName(db, "young.thug")
	sc := stream_command.CreateFromName(db, "damn")
	player.AllowAccess(db, p.ID, sc.ID)
	res := player.AllowedToPlay(db, p.ID, sc.ID)
	if !res {
		t.Error("young.thug SHOULD be allowed to play Damn")
	}

	player.RemoveAccess(db, p.ID, sc.ID)
	res = player.AllowedToPlay(db, p.ID, sc.ID)
	if res {
	}
}

func TestAddACommand(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	p := player.CreatePlayerFromName(db, "young.thug")
	_ = stream_command.CreateFromName(db, "damn")

	if p.ID == 0 {
		t.Error("Not returning Player ID")
	}
	allowedToPlay := UserAllowedToPlay(db, "young.thug", "damn")
	if allowedToPlay {
		t.Error("young.thug should NOT be allowed to play damn")
	}

	AllowUserAccessToCommand(db, "young.thug", "damn")
	allowedToPlay = UserAllowedToPlay(db, "young.thug", "damn")
	if !allowedToPlay {
		t.Error("young.thug SHOULD be allowed to play damn")
	}
}

func TestAddACommandWithIds(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	p := player.CreatePlayerFromName(db, "young.thug")
	sc := stream_command.CreateFromName(db, "damn")

	res := player.AllowedToPlay(db, p.ID, sc.ID)
	if res {
		t.Error("young.thug should NOT be allowed to play Damn")
	}

	player.AllowAccess(db, p.ID, sc.ID)
	res = player.AllowedToPlay(db, p.ID, sc.ID)
	if !res {
		t.Error("young.thug SHOULD be allowed to play Damn")
	}
}

func TestFindCommandCount(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	username := "beginbot"

	username2 := "young.thug"
	p := player.CreatePlayerFromName(db, username)
	p2 := player.CreatePlayerFromName(db, username2)
	fmt.Printf("p = %+v\n", p)
	fmt.Printf("p2 = %+v\n", p2)
	commandname := "damn"
	c := stream_command.CreateFromName(db, commandname)
	fmt.Printf("c = %+v\n", c)

	count := player.FindCommandCount(db, username)
	if count != 0 {
		t.Errorf("We should have access to 0 commands: %d", count)
	}

	Allow(db, p.ID, c.ID)
	Allow(db, p2.ID, c.ID)

	count = player.FindCommandCount(db, username)
	if count != 1 {
		t.Errorf("We should have access to 1 commands: %d", count)
	}
}
