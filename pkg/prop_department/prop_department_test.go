package prop_department

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// Add all
// Make a better test
// Fix Negative Props
func TestGiveProps(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	playername1 := "beginbot"
	playername2 := "young.thug"
	p1 := player.Player{Name: playername1, StreetCred: 1}
	p2 := player.Player{Name: playername2}

	db.Create(&p1)
	db.Create(&p2)

	parsedCmd := chat.ParsedCommand{
		Username:     playername1,
		TargetUser:   playername2,
		TargetAmount: 1,
	}
	_, err := GiveProps(db, &p1, &parsedCmd)

	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	yt := player.Find(db, playername2)
	if yt.CoolPoints != 1 {
		t.Errorf("%s should have 1 Cool Points: %d", playername2, yt.CoolPoints)
	}

	bb := player.Find(db, playername1)
	if bb.StreetCred != 0 {
		t.Errorf("%s should have 0 Street Cred: %d", playername1, bb.StreetCred)
	}
}

func TestSplittingPropsAmoungLovers(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	// So we make these 2, because go tests Run in parallel
	// by default and these were failing running the whole
	// sweet, so just give them 2s and everything passes!
	playername1 := "beginbot2"
	playername2 := "young.thug2"
	playername3 := "lil.wayne2"
	p1 := &player.Player{Name: playername1, StreetCred: 10}
	p2 := &player.Player{Name: playername2}
	p3 := &player.Player{Name: playername3}

	db.Create(&p1)
	db.Create(&p2)
	db.Create(&p3)

	pl1 := relationship_manager.PlayerLover{PlayerID: p1.ID, LoverID: p2.ID}
	pl2 := relationship_manager.PlayerLover{PlayerID: p1.ID, LoverID: p3.ID}
	db.Table("players_lovers").Create(&pl1)
	db.Table("players_lovers").Create(&pl2)

	parsedCmd := chat.ParsedCommand{Username: playername1, TargetAmount: 10}
	_, err := GiveProps(db, p1, &parsedCmd)
	if err != nil {
		fmt.Printf("Error giving Props: %+v\n", err)
	}

	p1 = player.Find(db, playername1)
	p2 = player.Find(db, playername2)
	p3 = player.Find(db, playername3)

	if p1.StreetCred != 0 {
		t.Errorf("%s Should have 0 Street Cred: %d", p1.Name, p1.StreetCred)
	}

	if p2.CoolPoints != 5 {
		t.Errorf("%s Should have 5 Cool Point: %d", p2.Name, p2.CoolPoints)
	}

	if p3.CoolPoints != 5 {
		t.Errorf("%s Should have 5 Cool Point: %d", p3.Name, p3.CoolPoints)
	}
}

func TestGivePropsToYourself(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	playername := "young.thug"
	p1 := &player.Player{Name: playername, StreetCred: 10}
	db.Create(&p1)
	parsedCmd := chat.ParsedCommand{TargetUser: "young.thug"}
	res, err := GiveProps(db, p1, &parsedCmd)
	fmt.Printf("res = %+v\n", res)
	if err == nil {
		t.Errorf("You should NOT be able to props yourself")
	}

	msg := "@young.thug you cannot !props yourself!"
	if res != msg {
		t.Errorf("Incorrect Error Msg: %v", res)
	}
}

func TestGiveAllProps(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	playername := "young.thug"
	p1 := &player.Player{Name: playername, StreetCred: 10}
	db.Create(&p1)
	parsedCmd := chat.ParsedCommand{TargetUser: "young.thug"}
	res, err := GiveProps(db, p1, &parsedCmd)
	fmt.Printf("res = %+v\n", res)
	if err == nil {
		t.Errorf("You should NOT be able to props yourself")
	}

	msg := "@young.thug you cannot !props yourself!"
	if res != msg {
		t.Errorf("Incorrect Error Msg: %v", res)
	}
}
