package irc

import (
	"context"
	"strings"
	"testing"
)

func TestReadIrc(t *testing.T) {
	r := strings.NewReader("some io.Reader stream to be read\nHello")
	ctx := context.Background()
	messages := ReadIrc(ctx, r)

	res := <-messages
	if res != "some io.Reader stream to be read\n" {
		t.Error("We did not read the message back")
	}
	res2 := <-messages
	if res2 != "Hello" {
		t.Error("We did not read the message back")
	}
}
