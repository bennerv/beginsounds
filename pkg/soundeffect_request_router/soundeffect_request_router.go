package soundeffect_request_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_parser"
	"gorm.io/gorm"
)

// TODO: Refactor this!!!
func Route(
	ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	results := make(chan string)
	botbotResults := make(chan string)

	go func() {
		defer close(results)
		defer close(botbotResults)

	MsgLoop:
		for msg := range commands {
			select {
			case <-ctx.Done():
				return
			default:
				parsedCmd := msg.ParsedCmd
				parts := msg.Parts

				switch parsedCmd.Name {
				case "deny":
					if !msg.Streamlord {
						continue MsgLoop
					}

					if parsedCmd.TargetAmount != 0 {
						err := soundeffect_request.UnapproveForPlayer(
							db,
							parsedCmd.TargetAmount,
							msg.PlayerID,
						)
						if err != nil {
							fmt.Printf("Error Denying %d - %v",
								parsedCmd.TargetAmount,
								msg.PlayerID,
							)
						}

					}

				case "soundeffect":
					if len(parts) < 2 {
						results <- "!soundeffect YOUTUBE_URL COMMAND_NAME 00:01 00:05 - Must be less than 5 second"
						continue MsgLoop
					}
					fmt.Printf("\tAttempting to save sound: %s", parsedCmd.Name)

					// Do we want to build another Parser, a more specific soundeffect request
					err, sfxRequest := soundeffect_request_parser.Parse(msg)
					if err != nil {
						fmt.Printf("err = %+v\n", err)
						continue MsgLoop
					}
					err = soundeffect_request.Save(db, sfxRequest)
					if err != nil {
						fmt.Printf("err = %+v\n", err)
						continue MsgLoop
					}

					results <- fmt.Sprintf(
						"Thank you for you soundeffect request %s @%s. Streamlords will review it promptly",
						sfxRequest.Name,
						msg.PlayerName,
					)

				case "requests":
					if !msg.Streamgod && !msg.Streamlord {
						continue MsgLoop
					}

					unapproved, _ := soundeffect_request.Unapproved(db)

					if len(unapproved) == 0 {
						msg := fmt.Sprintf("Excellent Jobs Stream Lords No Soundeffect Requests!")
						fmt.Println(msg)
						results <- msg
						continue MsgLoop
					}

					for _, res := range unapproved {
						p := player.Player{ID: msg.PlayerID}
						tx := db.First(&p)
						rID := res.RequesterID.Int32
						requester := player.FindByID(db, int(rID))
						if tx.Error == nil {
							req := fmt.Sprintf(
								"ID: %d - !%s %s @%s | %v - %v",
								res.ID,
								res.Name,
								res.FormattedURL(),
								requester.Name,
								res.StartTime,
								res.EndTime,
							)
							botbotResults <- req
						}
					}
					continue MsgLoop

				case "approvetheme":
					if !msg.Streamlord {
						results <- fmt.Sprintf("Only Streamlords can approve @%s", msg.PlayerName)
						continue MsgLoop
					}

					if parsedCmd.TargetUser != "" {
						botbotResults <- fmt.Sprintf(
							"Must provide value User name to approve theme @%s",
							msg.PlayerName,
						)
						continue MsgLoop
					}

					err := soundeffect_request.ApproveByName(db, parsedCmd.TargetUser, msg.PlayerID)
					if err != nil {
						results <- fmt.Sprintf("Error Approving: %+v\n", err)
						continue MsgLoop
					}
					results <- fmt.Sprintf(
						"@%s Approved Theme for @%s",
						msg.PlayerName,
						parsedCmd.TargetUser,
					)

				case "approve":
					if !msg.Streamlord {
						results <- fmt.Sprintf("Only Streamlords can approve @%s", msg.PlayerName)
						continue MsgLoop
					}

					if parsedCmd.TargetAmount != 0 {
						err := soundeffect_request.ApproveByID(
							db,
							parsedCmd.TargetAmount,
							msg.PlayerID,
						)
						if err != nil {
							fmt.Printf("Error Approving: %+v\n", err)
						}
						results <- fmt.Sprintf("Approving Soundeffect ID: %d", parsedCmd.TargetAmount)
						continue MsgLoop
					}

					if parsedCmd.TargetUser != "" {
						p := player.Find(db, parsedCmd.TargetUser)
						err := soundeffect_request.ApproveForPlayer(db, p.ID, msg.PlayerID)
						if err != nil {
							results <- fmt.Sprintf("Error Approving: %+v\n", err)
							continue MsgLoop
						}
						results <- fmt.Sprintf("Approving Soundeffects for @%s", p.Name)
						continue MsgLoop
					}

					potentialSoundeffect := parts[1]
					err := soundeffect_request.ApproveByName(db, potentialSoundeffect, msg.PlayerID)
					if err != nil {
						results <- fmt.Sprintf("Error Approving: %+v\n", err)
						continue MsgLoop
					}
					results <- fmt.Sprintf("Approving Soundeffects for @%s", potentialSoundeffect)
					continue MsgLoop
				}
			}
		}

	}()

	return botbotResults, results
}
