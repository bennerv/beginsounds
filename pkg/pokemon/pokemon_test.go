package pokemon

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestCurrentAnswer(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	answer := CurrentAnswer(db)
	if answer.Pokemon == "" {
		t.Errorf("Error creating Answer")
	}
	if answer.ID == 0 {
		t.Errorf("Error returning ID")
	}

	answer = PokemonAnswer{Pokemon: "onix"}
	db = db.Create(&answer)
	if db.Error != nil {
		t.Errorf("Error Saving %+v", db.Error)
	}

	if answer.Pokemon != "onix" {
		t.Errorf("Error Finding Answer %v", answer)
	}
}

func TestCurrentGuessCount(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	guessCount := CurrentGuessCount(db)
	fmt.Println("Guess Count: ", guessCount)
	if guessCount != 0 {
		t.Errorf("We shouldn't have any guesses")
	}

	msg := chat.ChatMessage{
		PlayerName: "Young.Thug",
		Message:    "!guess charizard"}

	guess, correct, guessCount := CheckGuess(db, msg)
	if guess != "charizard" {
		t.Errorf("WE found the wrong guess")
	}
	if correct {
		t.Errorf("That wasn't a correct guess!")
	}
	if guessCount != 1 {
		t.Errorf("We should have 1 guess: %d", guessCount)
	}

	answer := CurrentAnswer(db)
	winningMsg := chat.ChatMessage{
		PlayerName: "Young.Thug",
		Message:    fmt.Sprintf("!guess %s", answer.Pokemon)}

	guess, correct, guessCount = CheckGuess(db, winningMsg)
	if !correct {
		t.Error("We should have guessed correct!")
	}
	if guessCount != 2 {
		t.Errorf("Wrong Guess Count. Expected 2 | Got %d", guessCount)
	}
}

func TestCreatingPokemonGuesses(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	a := CreatePokemonAnswer(db, "grimer")
	p := player.CreatePlayerFromName(db, "young.thug")
	g := CreatePokemonGuess(db, "muk", p.ID, a.ID)

	wonAt := time.Now()
	db = db.Table("pokemon_answers").Where("pokemon ILIKE ?", a.Pokemon).Update("won_at", &wonAt)
	fmt.Printf("g = %+v\n", g)
}

// p := player.FindOrCreate(db, chatMsg.Username)
// userGuess := PokemonGuess{
// 	Pokemon:         guess,
// 	PlayerId:        int(p.ID),
// 	PokemonAnswerId: answer.Id}
