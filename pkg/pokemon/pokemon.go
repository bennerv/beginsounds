package pokemon

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

type PokemonAnswer struct {
	ID        int
	Pokemon   string
	CreatedAt *time.Time
	WonAt     *time.Time
}

type PokemonGuess struct {
	ID              int `gorm:"primaryKey"`
	Pokemon         string
	CreatedAt       time.Time
	PokemonAnswer   *PokemonAnswer `gorm:"-"`
	PokemonAnswerID int
	Player          *player.Player `gorm:"foreignKey:PlayerID"`
	PlayerID        int
}

func CreatePokemonGuess(db *gorm.DB,
	guess string,
	playerID int,
	AnswerID int) *PokemonGuess {

	g := PokemonGuess{Pokemon: guess, PlayerID: int(playerID), PokemonAnswerID: int(AnswerID)}
	db = db.Create(&g)
	if db.Error != nil {
		fmt.Printf("NewNew: Err Saving Pokemon Guess: %+v", db.Error)
	}
	return &g
}

func CreatePokemonAnswer(db *gorm.DB, name string) *PokemonAnswer {
	answer := PokemonAnswer{Pokemon: name}
	db = db.Create(&answer)
	if db.Error != nil {
		fmt.Printf("NewNew: Err Saving Pokemon Answer: %+v", db.Error)
	}
	return &answer
}

func CheckGuess(db *gorm.DB, chatMsg chat.ChatMessage) (string, bool, int64) {
	answer := CurrentAnswer(db)
	guess := extractGuess(chatMsg.Message)
	correct := guess == answer.Pokemon

	p := player.FindOrCreate(db, chatMsg.PlayerName)
	userGuess := CreatePokemonGuess(db, guess, p.ID, answer.ID)
	fmt.Printf("userGuess = %+v\n", userGuess)

	var winnerCount int64
	winnerCount = CurrentGuessCount(db)

	if correct {
		fmt.Println("\tUpdating the Pokemon answer")

		wonAt := time.Now()
		db = db.Table("pokemon_answers").
			Where("pokemon ILIKE ?", answer.Pokemon).Update("won_at", &wonAt)

		if db.Error != nil {
			fmt.Printf("Error Updating WonAt err = %+v\n", db.Error)
		}

	}

	return guess, correct, winnerCount
}

func CurrentAnswer(db *gorm.DB) PokemonAnswer {
	var answer PokemonAnswer
	winnerDB := db.Model(&answer).Where("won_at IS NULL").First(&answer)

	if winnerDB.Error != nil {
		pokemonName := randomGuess()
		answer := PokemonAnswer{Pokemon: pokemonName}
		aDB := db.Create(&answer)

		if aDB.Error != nil {
			fmt.Printf("NewCurrentAnswer: Err Saving Pokemon Answer: %+v", aDB.Error)
		}
		return answer
	}

	return answer
}

func CurrentGuessCount(db *gorm.DB) int64 {
	answer := CurrentAnswer(db)

	var guesses []*PokemonGuess
	var count int64
	db.Table("pokemon_guesses").Where("pokemon_answer_id = ?", answer.ID).Model(&guesses).Count(&count)

	return count
}

func randomGuess() string {
	pokemon_choices := [151]string{"bulbasaur", "ivysaur", "venusaur", "charmander", "charmeleon", "charizard", "squirtle", "wartortle", "blastoise", "caterpie", "metapod", "butterfree", "weedle", "kakuna", "beedrill", "pidgey", "pidgeotto", "pidgeot", "rattata", "raticate", "spearow", "fearow", "ekans", "arbok", "pikachu", "raichu", "sandshrew", "sandslash", "nidoran-f", "nidorina", "nidoqueen", "nidoran-m", "nidorino", "nidoking", "clefairy", "clefable", "vulpix", "ninetales", "jigglypuff", "wigglytuff", "zubat", "golbat", "oddish", "gloom", "vileplume", "paras", "parasect", "venonat", "venomoth", "diglett", "dugtrio", "meowth", "persian", "psyduck", "golduck", "mankey", "primeape", "growlithe", "arcanine", "poliwag", "poliwhirl", "poliwrath", "abra", "kadabra", "alakazam", "machop", "machoke", "machamp", "bellsprout", "weepinbell", "victreebel", "tentacool", "tentacruel", "geodude", "graveler", "golem", "ponyta", "rapidash", "slowpoke", "slowbro", "magnemite", "magneton", "farfetchd", "doduo", "dodrio", "seel", "dewgong", "grimer", "muk", "shellder", "cloyster", "gastly", "haunter", "gengar", "onix", "drowzee", "hypno", "krabby", "kingler", "voltorb", "electrode", "exeggcute", "exeggutor", "cubone", "marowak", "hitmonlee", "hitmonchan", "lickitung", "koffing", "weezing", "rhyhorn", "rhydon", "chansey", "tangela", "kangaskhan", "horsea", "seadra", "goldeen", "seaking", "staryu", "starmie", "mr.mime", "scyther", "jynx", "electabuzz", "magmar", "pinsir", "tauros", "magikarp", "gyarados", "lapras", "ditto", "eevee", "vaporeon", "jolteon", "flareon", "porygon", "omanyte", "omastar", "kabuto", "kabutops", "aerodactyl", "snorlax", "articuno", "zapdos", "moltres", "dratini", "dragonair", "dragonite", "mewtwo", "mew"}

	rand.Seed(time.Now().UnixNano())
	rand_pokemon := pokemon_choices[rand.Intn(151)]
	return rand_pokemon
}

func extractGuess(msg string) string {
	return strings.Split(msg, " ")[1]
}
