package economy_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/giver"
	"gorm.io/gorm"
)

func GivingRouter(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (chan string, chan string) {

	beginbotResponses := make(chan string)
	beginbotbotResponses := make(chan string)

	go func() {
		defer close(beginbotResponses)
		defer close(beginbotbotResponses)

		for msg := range messages {
			switch msg.ParsedCmd.Name {

			case "clone", "share":
				res, err := giver.Clone(db, msg)
				if err == nil {
					beginbotResponses <- res
				} else {
					beginbotResponses <- fmt.Sprintf("Error Clone: %+v", err)
				}
			// Giving / Donating gives away command
			// at no cost
			case "give", "donate":
				res, err := giver.Giver(
					db,
					msg,
				)
				if err == nil {
					beginbotResponses <- res
				} else {
					beginbotResponses <- fmt.Sprintf("Error Giving: %+v", err)
				}
			}
		}
	}()

	return beginbotbotResponses, beginbotResponses
}
