package economy_router

import (
	"context"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/chat_parser"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// !createpack [packname] [sound1] [sound 2]
// !createpack karen
//
// !addtopack karen [sound1] [sound2]
// !removefrompack
func TestRoutingPacks(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	ctx := context.Background()

	chatter := player.CreatePlayerFromName(db, "young.thug")
	msg := "!createpack party"

	msg1, _ := chat_parser.Parse(db, chatter, msg)

	messages := make(chan chat.ChatMessage, 1)
	messages <- msg1

	botbot, _ := PackRouter(ctx, db, messages)

	timer := time.NewTimer(time.Millisecond * 10)
	select {
	case <-timer.C:
		t.Errorf("We expected message back")
	case res := <-botbot:
		if res != "New Pack: party" {
			t.Errorf("Didn't get expected message back: %s", res)
		}
	}

	msg2 := chat.ChatMessage{
		PlayerID:   chatter.ID,
		PlayerName: chatter.Name,
		Streamgod:  true,
		Message:    "!addtopack party damn",
	}

	messages <- msg2
	botbot, _ = PackRouter(ctx, db, messages)

	timer = time.NewTimer(time.Millisecond * 10)
	select {
	case <-timer.C:
		// t.Errorf("We expected message back")
	case res := <-botbot:
		if res != "New Sound in \"party\": !damn" {
			t.Errorf("Didn't get expected message back: %s", res)
		}
	}

	// Do I want to do approvals for streamlords
	// We want to make sure a Pack was created
	// want to make sure admin got a message back
}
