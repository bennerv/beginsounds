package economy_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gorm.io/gorm"
)

// TODO:
// 	refactor this!
//
// BuyRoute handles user requests for buying
func BuyRoute(ctx context.Context,
	db *gorm.DB,
	commands <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

		for msg := range commands {
			parsedCmd := msg.ParsedCmd

			switch parsedCmd.Name {
			case "buy":
				res, err := Buyer(
					db,
					msg,
					parsedCmd.TargetCommand,
					parsedCmd.TargetAmount,
				)

				if err != nil {
					m := fmt.Sprintf("Error Buying: %+v", err)
					results <- m
				}

				if res != "" {
					results <- res
				}
			}
		}
	}()

	return results
}
