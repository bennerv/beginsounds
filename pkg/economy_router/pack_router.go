package economy_router

import (
	"context"
	"fmt"
	"log"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// PackRouter routes commands related to the purchasing
//  and distribution of packs
func PackRouter(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	botbotResponses := make(chan string)
	chatResponses := make(chan string)

	go func() {
		for msg := range messages {
			parsedCmd := msg.ParsedCmd
			parts := msg.Parts

			switch parsedCmd.Name {
			case "peakpack", "peekpack":
				if parsedCmd.TargetPackName == "" {
					chatResponses <- fmt.Sprintf(
						"@%s you tried to peak at Pack that isn't present: %s",
						msg.PlayerName,
						parts[1],
					)
					continue
				}

				// fmt.Printf("\tparsedCmd.TargetPack = %+v\n", parsedCmd.TargetPack)
				// cmds := parsedCmd.TargetPack.Commands(db)

				p, _ := pack.Find(db, parsedCmd.TargetPackName)
				cmds := p.Commands(db)

				chatResponses <- fmt.Sprintf(
					"@%s Pack: %s | %s",
					msg.PlayerName,
					parsedCmd.TargetPackName,
					strings.Join(cmds, " | "),
				)

			case "listpacks":
				names, err := pack.AllNames(db)
				if err == nil {
					site := "http://www.beginworld.exchange/packs.html"
					m := fmt.Sprintf("Packs: %s | %s", strings.Join(names, ", "), site)
					chatResponses <- m
					pack.GeneratePacksPage(db)
					website_generator.SyncRootPage("packs")
				}
			case "createpack":
				p, err := pack.CreatePackFromParts(db, parts[1:])
				// Should we pass this to the pack builder
				// botbotResponses <- "New Pack party"
				if err == nil {
					botbotResponses <- fmt.Sprintf("New Pack: %s", p.Name)
				}
			case "addtopack":
				packName := parts[1]
				cps, _ := pack.AddSoundsToPacks(db, packName, parts[2:])
				for _, command := range cps {
					botbotResponses <- fmt.Sprintf(
						"New Command: %s Added to Pack: %s",
						command,
						packName,
					)
				}
				// s = "New Sound in \"party\": !damn"
			case "removefrompack":
				// case "approvepack":
				// case "denypack":

			case "droppack":
				potentialPack := parts[1]
				p, _ := pack.FindByName(db, potentialPack)
				// playa := player.FindByName(db, parsedCmd.TargetUser)
				p.GiveToPlayer(db, parsedCmd.TargetUser)
				chatResponses <- fmt.Sprintf(
					"@%s got pack: %s",
					parsedCmd.TargetUser,
					potentialPack,
				)

			case "buypack":
				if parsedCmd.TargetPackName != "" {
					tp, err := pack.FindByName(db, parsedCmd.TargetPackName)
					cmds := pack.CommandsInPack(db, tp.ID)
					var totalCost int
					for _, cmd := range cmds {
						totalCost = totalCost + cmd.Cost
					}

					if msg.CoolPoints < totalCost {
						chatResponses <- fmt.Sprintf(
							"@%s doesn't have enough Cool Points to Buy Pack: %s | %d/%d",
							msg.PlayerName,
							parsedCmd.TargetPackName,
							msg.CoolPoints,
							totalCost,
						)
						continue
					}

					if err != nil {
						log.Print(err)
					}

					pp, err := pack.Give(db, tp.ID, msg.PlayerID)

					if pp.PlayerID != 0 {
						err := db.Model(&player.Player{ID: pp.PlayerID}).
							Update("cool_points", msg.CoolPoints-totalCost)
						if err != nil {
							fmt.Printf("err = %+v\n", err)
							chatResponses <- fmt.Sprintf("@%s Error buying bought Pack: %s", msg.PlayerName, tp.Name)
							continue
						}

						chatResponses <- fmt.Sprintf("@%s bought Pack: %s", msg.PlayerName, tp.Name)
					}

					if err != nil {
						msg := fmt.Sprintf("@%s Error buying Pack: %s", msg.PlayerName, tp.Name)
						chatResponses <- msg
					}

					// We need to iterate and charge
					// but we also already should check if they can afford

					// We can handle

				}
			}
		}
	}()

	return botbotResponses, chatResponses
}
