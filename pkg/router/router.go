package router

import (
	"context"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/chat_parser"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

func FilterChatMsgs(
	ctx context.Context,
	db *gorm.DB,
	c *config.Config,
	messages <-chan string,
) <-chan chat.ChatMessage {

	UsersWeDoNotLike := []string{"nightbot"}

	userMsgs := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(userMsgs)

		for msg := range messages {
		outside:

			select {
			case <-ctx.Done():
				return
			default:
				welcomePack, _ := pack.Find(db, "welcome")

				if parser.IsPrivmsg(msg) {
					user, m := parser.ParsePrivmsg(msg)
					// If this creates a new user
					// We should give them the welcome pack
					p, newPlayer := player.FindOrWelcome(db, user)

					if newPlayer {
						// TODO come back and add this
						// give them the welcome pack
						// chatMessage <- "@%s Welcome to Beginworld: Welcome Gifts: !hello !clap"
						pack.Give(db, welcomePack.ID, p.ID)
					}
					chatMessage, _ := chat_parser.Parse(db, p, m)

					for _, u := range UsersWeDoNotLike {
						if u == user {
							break outside
						}
					}

					userMsgs <- chatMessage

				} else if parser.IsPing(msg) {
					irc.Pong(c)
				}

			}
		}
	}()

	return userMsgs
}
