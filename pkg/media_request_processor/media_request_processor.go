package media_request_processor

import (
	"context"
	"fmt"
	"os/exec"
	"sync"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
	"gorm.io/gorm"
)

// Process looks for approved Media Request
// 	attempts to download them and make them
// available on stream
func Process(
	ctx context.Context,
	db *gorm.DB,
) (<-chan string, <-chan string, <-chan chat.ChatMessage) {

	timer := time.NewTicker(time.Second * 25)

	chatResults := make(chan string)
	botbotResults := make(chan string)
	obsResults := make(chan chat.ChatMessage)

	go func() {
		defer close(chatResults)
		defer close(botbotResults)
		defer close(obsResults)

		fmt.Println("Launching The Media Processor")

		// MsgLoop:
		for {
			select {
			case <-ctx.Done():
				return
			default:
				var wg sync.WaitGroup

				videos, err := media_request.ApprovedVideos(db)
				if err != nil {
					fmt.Printf("Err Fetching Approved Videos = %+v\n", err)
				}

				images, err := media_request.ApprovedImages(db)
				if err != nil {
					fmt.Printf("Err Fetching Approved images %+v\n", err)
				}

				unprocessedMedia := len(videos) + len(images)
				wg.Add(unprocessedMedia)

				go ProcessVideos(db, &wg, videos, botbotResults, obsResults)
				go ProcessImage(db, &wg, images, botbotResults, obsResults)

				wg.Wait()

				<-timer.C
			}
		}
	}()

	return botbotResults, chatResults, obsResults
}

func ProcessImage(
	db *gorm.DB,
	wg *sync.WaitGroup,
	images []media_request.MediaRequest,
	botbotResults chan<- string,
	obsResults chan<- chat.ChatMessage,
) {

	for _, image := range images {

		fmt.Printf("\timage.Name = %+v\n", image.Name)
		if image.Filename == "" {
			fmt.Printf("Missing Filename: %+v\n", image.Name)
		}

		filename := fmt.Sprintf(
			"/home/begin/stream/Stream/ViewerImages/%s",
			image.Filename,
		)
		// s3cmd --no-mime-magic --acl-public --delete-after --recursive put \
		// 	/home/begin/stream/Stream/ViewerImages \
		// 	s3://beginworld/memes/

		fmt.Print("\nStarting download\n", filename)
		err := utils.DownloadFile(image.URL, filename)
		fmt.Print("\nEnding download\n")

		if err != nil {
			msg := fmt.Sprintf(
				"Error Downloading: %s | %+v",
				image.Name,
				err,
			)
			fmt.Print(msg)
			botbotResults <- msg
			wg.Done()
		}

		m := fmt.Sprintf("!create_image_source %s", image.Name)
		cm := chat.ChatMessage{
			Message:    m,
			PlayerName: "beginbotbot",
			Streamgod:  true,
			Streamlord: true,
		}
		obsResults <- cm
		wg.Done()

	}

}

func ProcessVideos(
	db *gorm.DB,
	wg *sync.WaitGroup,
	videos []media_request.MediaRequest,
	botbotResults chan<- string,
	obsResults chan<- chat.ChatMessage,
) {

VideoLoop:
	for _, video := range videos {

		// We should strip off any part of the
		// name outside of name.whateve
		filename := fmt.Sprintf(
			"/home/begin/stream/Stream/ViewerVideos/%s", video.Filename)
		err := utils.DownloadFile(video.URL, filename)

		if err != nil {
			msg := fmt.Sprintf(
				"err Downloading: %s | %s %+v\n",
				video.Name,
				video.URL,
				err,
			)
			fmt.Println(msg)
			botbotResults <- msg
			wg.Done()
			continue VideoLoop
		}

		args := []string{
			"--no-mime-magic",
			"--acl-public",
			"put",
			filename,
			"s3://beginworld/memes/",
		}
		cmd := exec.Command("s3cmd", args...)
		err = cmd.Run()
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}

		m := fmt.Sprintf("!create_video_source %s", video.Name)
		cm := chat.ChatMessage{
			Message:    m,
			PlayerName: "beginbotbot",
			Streamgod:  true,
			Streamlord: true,
		}
		obsResults <- cm
		wg.Done()
	}
}
