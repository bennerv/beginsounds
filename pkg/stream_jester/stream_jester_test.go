package stream_jester

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
	"gorm.io/gorm"
)

func TestNewSecret(t *testing.T) {
	// db := database.CreateDBConn("test_beginsounds3")
	// NewSecret(db)
}

func TestCurrentJesterAndChaosMode(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	stream_command.CreateFromName(db, "damn")

	res, _ := CurrentJester(db)
	if res.PlayerID != 0 {
		t.Errorf("Shouldn't have a current jester")
	}

	if res.ChaosMode {
		t.Error("We Should not be in Chaos mode!")
	}

	p := player.CreatePlayerFromName(db, "young.thug")
	sj := StreamJester{PlayerID: p.ID}
	tx := db.Create(&sj)
	if tx.Error != nil {
		fmt.Printf("Error Creating Jester = %+v\n", tx.Error)
	}
	res, err := CurrentJester(db)

	if err != nil {
		t.Error(err)
		return
	}

	if res.PlayerID == 0 {
		t.Errorf("Should have a current jester")
	}

	if res.ChaosMode {
		t.Error("We should NOT be in Chaos Mode")
	}
	_, _ = ChaosMode(db, fakeSync)

	cj, err := CurrentJester(db)
	if !cj.ChaosMode {
		t.Error("We should have flipped into Chaos Mode")
	}

	_, _ = ChaosMode(db, fakeSync)

	cj, err = CurrentJester(db)
	if cj.ChaosMode {
		t.Error("We should have turned Chaos Mode back off")
	}
}

// This is to be passed in, to prevent actually Syncing
// during tests
func fakeSync(db *gorm.DB, sj *StreamJester) {
}
