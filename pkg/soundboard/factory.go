package soundboard

import (
	"errors"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func CreateAudioRequestFromSFXRequest(
	db *gorm.DB,
	sr *soundeffect_request.SoundeffectRequest,
) (*audio_request.AudioRequest, error) {

	p := player.FindByID(db, int(sr.RequesterID.Int32))

	msg := chat.ChatMessage{
		Message:    fmt.Sprintf("!%s", sr.Name),
		PlayerID:   p.ID,
		PlayerName: p.Name,
		Streamgod:  p.Streamgod,
		Streamlord: p.Streamlord,
	}
	return CreateAudioRequestForNewSound(db, &msg)
}

func CreateAudioRequestForNewSound(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*audio_request.AudioRequest, error) {

	potentialSample := msg.Message[1:]

	if msg.PlayerID == 0 {
		return &audio_request.AudioRequest{}, nil
	}

	p := player.FindByID(db, msg.PlayerID)

	filename, path := findSoundPathInfo(potentialSample)
	// fmt.Printf("\tPotential Filename: %s\n", filename)

	if filename == "" || path == "" {
		// e := errors.New("Could not find the sample!")
		// fmt.Printf("Error Finding New Sample = %+v\n", e)
		return &audio_request.AudioRequest{}, nil
	}

	aq := audio_request.AudioRequest{
		PlayerID:   msg.PlayerID,
		Name:       potentialSample,
		Streamlord: p.Streamlord,
		Notify:     true,
		Filename:   filename,
		Path:       path,
		Free:       true,
	}

	audio_request.Save(db, &aq)

	return &aq, nil
}

func CreateAudioRequest(
	db *gorm.DB,
	potentialSample string,
	username string,
) (*audio_request.AudioRequest, error) {

	p := player.Find(db, username)

	msg := chat.ChatMessage{
		Message:    fmt.Sprintf("!%s", potentialSample),
		PlayerID:   p.ID,
		PlayerName: username,
		Streamgod:  p.Streamgod,
		Streamlord: p.Streamlord,
	}
	return CreateAudioRequestFromChatMsg(db, &msg)
}

// WE should create more decontrcuted version
// We could start following the Gopher
// Allowing a User to Play
func CreateAudioRequestFromChatMsg(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*audio_request.AudioRequest, error) {

	potentialSample := msg.Message[1:]

	soundsWeHate := []string{
		"pulse",
	}
	for _, sound := range soundsWeHate {
		if potentialSample == sound {
			return &audio_request.AudioRequest{}, nil
		}
	}

	// fmt.Printf("\tPotential Sample: %s\n", potentialSample)
	command := stream_command.Find(db, potentialSample)
	if command.Name == "" {
		return &audio_request.AudioRequest{}, nil
	}

	playerID := msg.PlayerID

	if playerID == 0 {
		// fmt.Printf("\tNo Player ID found for sample: %v\n", command)
		return &audio_request.AudioRequest{}, nil
	}

	filename, path := findSoundPathInfo(command.Name)
	// fmt.Printf("\tPotential Filename: %s\n", filename)

	if filename == "" || path == "" {
		// e := errors.New("Could not find the sample!")
		// fmt.Printf("Error Finding New Sample = %+v\n", e)
		return &audio_request.AudioRequest{}, nil
	}

	var allowedToPlay bool

	sj, _ := stream_jester.CurrentJester(db)
	// We don't have a stream jester

	jp := &player.Player{}
	if sj.PlayerID != 0 {
		jp = player.FindByID(db, sj.PlayerID)
	}

	// Is this wrong
	isJester := jp.ID == playerID

	// fmt.Printf("\tjp.ID %d | msg.PlayerID %d\n", jp.ID, playerID)

	if msg.Streamlord || msg.Streamgod || isJester || msg.PlayerName == command.Name {
		allowedToPlay = true
	} else {
		allowedToPlay = player.AllowedToPlay(db, playerID, command.ID)
	}

	if !allowedToPlay {
		e := errors.New(
			fmt.Sprintf("@%s not allowed to play !%s",
				msg.PlayerName,
				command.Name,
			),
		)
		return &audio_request.AudioRequest{}, e
	}

	aq := audio_request.AudioRequest{
		PlayerID:     playerID,
		Name:         command.Name,
		StreamJester: isJester,
		Streamlord:   msg.Streamlord,
		Notify:       true,
		Filename:     filename,
		Path:         path}

	audio_request.Save(db, &aq)

	return &aq, nil
}
