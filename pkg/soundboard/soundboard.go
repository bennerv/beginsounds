package soundboard

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func Execute(
	ctx context.Context,
	db *gorm.DB,
	sfxs <-chan audio_request.AudioRequest,
	noMemes <-chan bool,
) (
	<-chan string,
	<-chan string,
	<-chan chat.ChatMessage,
) {

	results := make(chan string)
	notifications := make(chan string)
	obsRequests := make(chan chat.ChatMessage)

	rand.Seed(time.Now().UnixNano())

	go func() {
		fmt.Println("\tLaunching the Soundboard")

		defer close(results)
		defer close(notifications)
		defer close(obsRequests)

	outside:
		for audioRequest := range sfxs {
			select {
			case <-ctx.Done():
				return
			default:

				// We need to check if always allowed to play
				// This means we can allow dynamic lengths of sample
				timer := player.FindTimerByID(db, audioRequest.PlayerID)

				j, _ := stream_jester.CurrentJester(db)
				var isJester bool
				if j.PlayerID == 0 {
					isJester = false
				} else {
					isJester = audioRequest.PlayerID == j.PlayerID
				}

				var p player.Player
				res := db.Table("players").Where("ID = ?", audioRequest.PlayerID).Find(&p)
				if res.Error != nil {
					res := fmt.Sprintf("Couldn't find User %v", p)
					fmt.Println(res)
					results <- res
					continue outside
				}

				// If you aren't streamgod OR jester
				// We have to stop and check your permissions
				if !p.Streamgod && !isJester {

					if p.Mana < 1 {
						if p.Name != "" {
							res := fmt.Sprintf("@%s does not have enough Mana to play !%s",
								p.Name, audioRequest.Name)
							fmt.Println(res)
							results <- res
						}
						continue outside
					}

					if p.InJail {
						results <- fmt.Sprintf("%s Is in Jail! You can't play sounds!", p.Name)
						continue outside
					}

				}

				// If you are past this point we assume you
				// have permission to play a sound

				if audioRequest.Notify {
					notifications <- fmt.Sprintf("@%s !%s", p.Name, audioRequest.Name)
				}

				packEffects := map[string][]string{
					"teej":     {"!teej1", "!teej2"},
					"prime":    {"!primeagen"},
					"melkey":   {"!melkman2", "!melkman"},
					"roxkstar": {"!roxkstar"},
					"dean":     {"!peak dean"},
				}

				potentialPack := pack.EffectForPackSound(db, audioRequest.Name)
				// How do I check if a key is in dah map
				if potentialPack != "" {
					obsCommands, ok := packEffects[potentialPack]

					if ok {
						i := rand.Intn(len(obsCommands))
						fmt.Printf("\t == Choosing OBS Index! %+v\n", i)
						obsCommand := obsCommands[i]
						fmt.Printf("\t == Choosing OBS Command! %+v\n", obsCommand)

						obsRequests <- chat.ChatMessage{
							Message:    obsCommand,
							PlayerName: "beginbotbot",
							Streamgod:  true,
							Streamlord: true,
						}
					}
				}

				fmt.Printf("Attempting to play Sound: %s\n", audioRequest.Name)
				success := PlayAudio(audioRequest, timer, noMemes)
				if success {
					db.Model(&audioRequest).Update("played", true)
					// We don't subtract Mana From Stream Gods
					if !p.Streamgod && !isJester && !audioRequest.Free {
						res := db.Exec("UPDATE players SET mana = mana - 1 WHERE ID = ?",
							audioRequest.PlayerID)
						if res.Error != nil {
							fmt.Printf("Error Updating Mana= %+v\n", res.Error)
						}
					}
				}

				// go func(
				// 	aq *audio_request.AudioRequest,
				// 	t *time.Timer,
				// 	pp *player.Player,
				// ) {
				// 	PlayAudio(*aq, t, noMemes)
				// }(&audioRequest, timer, &p)

			}
		}
	}()

	return results, notifications, obsRequests
}
