package reporter

import (
	"context"
	"fmt"
	"math"

	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
)

func PrimaryReport(
	ctx context.Context,
	messages <-chan string,
	primaryConfig *config.Config,
	botConfig *config.Config,
) {

	go func() {
		for msg := range messages {
			select {

			case <-ctx.Done():
				return
			default:
				msgs := msgSplitter(msg)
				msgLen := len(msgs)

				head := msgs[0]
				tail := msgs[1:]
				irc.SendMsg(primaryConfig, head)

			Loop:
				for i, m := range tail {
					if msgLen == 1 {
						irc.SendMsg(botConfig, m)
						break Loop
					}

					cm := fmt.Sprintf("%s %d/%d", m, i+1, msgLen)
					irc.SendMsg(botConfig, cm)
				}
			}
		}
	}()
}
func Report(ctx context.Context, messages <-chan string, c *config.Config) {
	go func() {
		for msg := range messages {
			select {

			case <-ctx.Done():
				return
			default:
				msgs := msgSplitter(msg)
				msgLen := len(msgs)

			Loop:
				for i, m := range msgs {
					if msgLen == 1 {
						irc.SendMsg(c, m)
						break Loop
					}

					cm := fmt.Sprintf("%s %d/%d", m, i+1, msgLen)
					irc.SendMsg(c, cm)
				}
			}
		}
	}()
}

func msgSplitter(input string) []string {
	limit := 475
	res := []string{}
	if len(input) < limit {
		return []string{input}
	}

	for l := len(input); l > 0; l -= limit {
		size := int(math.Min(float64(len(input)), float64(limit)))
		res = append(res, input[:size])
		input = input[size:]
	}
	return res
}
