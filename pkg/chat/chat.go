package chat

import (
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gorm.io/gorm"
)

func (p *ParsedCommand) String() string {
	return fmt.Sprintf("Name %s Username %s TargetUser %s TargetCommand %s TargetAmount %d Streamgod %t Streamlord %t",
		p.Name,
		p.Username,
		p.TargetUser,
		p.TargetCommand,
		p.TargetAmount,
		p.Streamgod,
		p.Streamlord,
	)
}

// This could take on more values from the chat.ChatMessage for ease of use
type ParsedCommand struct {
	Name            string
	Username        string
	TargetUser      string
	TargetUserID    int
	TargetCommand   string
	TargetCommandID int
	TargetMedia     string
	TargetPack      pack.Pack
	TargetPackName  string
	TargetPackID    int
	TargetAmount    int

	// StreetCred int
	Streamlord bool
	Streamgod  bool
}

// DBChan a Database connection and a channel full of Messages
type DBChan struct {
	DB       *gorm.DB
	Messages <-chan ChatMessage
}

type ChatMessage struct {
	PlayerName string
	PlayerID   int
	Message    string

	Streamlord bool `gorm:"-"`
	Streamgod  bool `gorm:"-"`
	StreetCred int  `gorm:"-"`
	// Move Cool Points to ParsedCommand
	CoolPoints int `gorm:"-"`

	ParsedCmd ParsedCommand `gorm:"-"`
	Parts     []string      `gorm:"-"`
}

// Count returns the numbers of chat messages
func Count(db *gorm.DB) int64 {
	var count int64
	db.Table("chat_messages").Count(&count)
	return count
}

func Save(db *gorm.DB, chat_message ChatMessage) error {
	tx := db.Create(&chat_message)
	if tx.Error != nil {
		fmt.Printf("Save Error = %+v\n", db.Error)
	}
	return tx.Error
}

type StreetCredMap struct {
	Name       string
	ID         int
	LoverCount int
}

// This Query could not be return every user with Their lover sound
func FetchStreetCredCounts(db *gorm.DB) []*StreetCredMap {
	var results []*StreetCredMap

	db.Table("players").Raw(`
	SELECT p.name, p.id, (count(pl) + 1) as lover_count FROM players p
	LEFT JOIN players_lovers pl ON p.ID = pl.lover_id
	INNER JOIN (
	  SELECT
	    COUNT(1), player_id
	    FROM chat_messages
	    WHERE chat_messages.created_at < (NOW() + interval '4 hour')
	    GROUP BY player_id
	) cm ON cm.player_id = p.id
	GROUP BY p.name, p.id
	ORDER BY lover_count DESC`).Scan(&results)

	return results
}

type ChatResult struct {
	ID   int
	Name string
}

func CurrentlyChatting(db *gorm.DB, currentPlayerID int) []ChatResult {
	var results []ChatResult

	tx := db.Table("players").Raw(`
		SELECT
			p.id as id, p.name as name
		FROM
			chat_messages cm
		INNER JOIN
			players p
		ON
			cm.player_id = p.ID
		WHERE
			cm.created_at > (NOW() - interval '10 minute')
		AND
			p.bot = false
		AND
			p.ID != ?
		GROUP BY
			p.name, p.id;
	`, currentPlayerID).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("RecentChatters Error: %+v\n", tx.Error)
	}

	return results
}

func RecentChatters(db *gorm.DB) []ChatResult {
	var results []ChatResult

	tx := db.Table("players").Raw(`
		SELECT
			p.id as id, p.name as name
		FROM
			chat_messages cm
		INNER JOIN
			players p
		ON
			cm.player_id = p.ID
		WHERE
			cm.created_at > (NOW() - interval '24 hour')
		GROUP BY
			p.name, p.id;
	`).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("RecentChatters Error: %+v\n", tx.Error)
	}

	return results
}

func GiveStreetCredToRecentChatters(db *gorm.DB) []*StreetCredMap {
	results := FetchStreetCredCounts(db)
	for _, r := range results {

		// this needs to be Raw
		tx := db.Exec(`
			UPDATE players
			SET street_cred = street_cred + ?
			WHERE id = ?`, r.LoverCount, r.ID)

		if tx.Error != nil {
			fmt.Printf("res.Error = %+v\n", tx.Error)
		}

	}
	return results
}

func HasChattedToday(db *gorm.DB, playername string) bool {
	// TODO: Make this configurable
	fourHours := time.Now().Add(-(time.Hour * 4))

	var cm ChatMessage
	db = db.Where("player_name = ? AND created_at > ?", playername, fourHours).
		First(&cm)

	if db.Error != nil {
		return false
	}

	return true
}
