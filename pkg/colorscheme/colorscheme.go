package colorscheme

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"gorm.io/gorm"
)

type NewThemeCount struct {
	Theme string
	Total int64
}

// How Many Votes a Theme Got
type ThemeCount struct {
	Theme string
	Count int
}

// How Many Times a User Voted for a Theme
type VoteCount struct {
	Theme    string
	Username string
	Count    int
}

// A User Voting for a Theme
type ColorschemeVote struct {
	Username string
	Theme    string
}

type Colorscheme struct {
	Theme   string
	Special Special
	Colors  Colors
}

type Special struct {
	Background string `json:"background"`
	Foreground string `json:"foreground"`
	Cursor     string `json:"cursor"`
}

type Colors struct {
	Color0  string `json:"color0"`
	Color1  string `json:"color1"`
	Color2  string `json:"color2"`
	Color3  string `json:"color3"`
	Color4  string `json:"color4"`
	Color5  string `json:"color5"`
	Color6  string `json:"color6"`
	Color7  string `json:"color7"`
	Color8  string `json:"color8"`
	Color9  string `json:"color9"`
	Color10 string `json:"color10"`
	Color11 string `json:"color11"`
	Color12 string `json:"color12"`
	Color13 string `json:"color13"`
	Color14 string `json:"color14"`
	Color15 string `json:"color15"`
}

func (c Colorscheme) String() string {
	return fmt.Sprintf("Color<%s>", c.Theme)
}

func Count(db *gorm.DB) int64 {
	var count int64
	db = db.Table("colorscheme_votes").Count(&count)
	if db.Error != nil {
		panic(db.Error)
	}
	return count
}

func VotesByUser(db *gorm.DB, theme string) []VoteCount {
	var results []VoteCount
	db.Table("colorscheme_votes").
		Where("theme = ?", theme).
		Select("username, count(*) AS total").
		Group("username").
		Order("total DESC").
		Find(&results)
	return results
}

func ThemesByCount(db *gorm.DB) []ThemeCount {
	var res []ThemeCount

	db.Table("colorscheme_votes").
		Select("theme, count(*) AS total").
		Group("theme").
		Order("total DESC").
		Find(&res)

	return res
}

func All(db *gorm.DB) []ColorschemeVote {
	var colorschemes []ColorschemeVote

	db = db.Table("colorscheme_votes").Find(&colorschemes)

	if db.Error != nil {
		fmt.Println("Error for All: ", db.Error)
	}

	return colorschemes
}

func NewWinningColor(db *gorm.DB) NewThemeCount {
	var winner NewThemeCount

	db.Table("colorscheme_votes").
		Select("theme, count(theme) as total").
		Group("theme").
		Order("total DESC").
		First(&winner)

	return winner
}

// gorm
func NewCountByTheme(db *gorm.DB, theme string) int64 {
	var count int64

	db.Model(&ColorschemeVote{}).Where("theme = ?", theme).Count(&count)
	// db.Table("colorscheme_votes").Where("theme = ?", theme).Count(&count)

	if db.Error != nil {
		fmt.Printf("NewCountByTheme: db.Error = %+v\n", db.Error)
		return count
	}

	return count
}

func NewCreate(db *gorm.DB, colorschemeVote ColorschemeVote) {
	db.Create(colorschemeVote)
}

func ParseColorScheme(theme string) Colorscheme {
	var colorscheme Colorscheme
	colorscheme.Theme = theme
	// Could Read this From Github
	filename := fmt.Sprintf("/home/begin/code/pywal/pywal/colorschemes/dark/%s.json", theme)
	colorData, _ := ioutil.ReadFile(filename)
	err := json.Unmarshal(colorData, &colorscheme)

	if err != nil {
		fmt.Println("ERRORS ", err)
	}

	return colorscheme
}

func GatherColors() []string {
	dir := "/home/begin/code/pywal/pywal/colorschemes/dark"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Println(err)
		return []string{}
	}

	themes := make([]string, len(files))
	hatedColors := []string{"base16-icy", "base16-greenscreen", "dkeg-owl"}

Loop:
	for i, f := range files {
		n := f.Name()
		name := strings.TrimSuffix(filepath.Base(n), filepath.Ext(n))
		for _, color := range hatedColors {
			if color == name {
				continue Loop
			}
		}
		themes[i] = name
	}

	return themes
}
