package parser

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gorm.io/gorm"
)

func ParseChatMessageNoJudgements(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*chat.ParsedCommand, []string, error) {

	var parsedCmd chat.ParsedCommand
	parsedCmd.Username = msg.PlayerName
	parsedCmd.Streamgod = msg.Streamgod
	parsedCmd.Streamlord = msg.Streamlord

	propsGiver := player.Find(db, msg.PlayerName)

	parts := strings.Split(msg.Message, " ")

	name := string(parts[0])
	fc := string(name[0])
	if fc == "@" || fc == "!" {
		name = name[1:]
	}
	parsedCmd.Name = strings.ToLower(name)

	for _, part := range parts[1:] {

		if len(part) == 0 {
			return &parsedCmd, []string{}, errors.New("Not parts after stripping message")
		}

		fc := string(part[0])
		if fc == "@" || fc == "!" {
			part = part[1:]
		}
		part = strings.ToLower(part)

		tp, _ := pack.FindByName(db, part)
		if tp.ID != 0 {
			// TODO: Explore why this isn't being passed to the pack_router
			parsedCmd.TargetPack = pack.Pack{ID: tp.ID, Name: tp.Name}
			parsedCmd.TargetPackName = tp.Name
			parsedCmd.TargetPackID = tp.ID
		}

		c := stream_command.FindNoTheme(db, part)
		if c.ID != 0 {
			parsedCmd.TargetCommand = c.Name
			parsedCmd.TargetCommandID = c.ID
		}

		// This find by name is not just query unappproved
		// m, _ := media_request.FindByName(db, part)
		m, _ := media_request.FindUnapprovedByName(db, part)
		if m.ID != 0 {
			parsedCmd.TargetMedia = m.Name
		}

		// We are checking the Street of the TargetUser
		altN := "송써니"
		if part == altN {
			part = "ssyuni"
		}
		p := player.Find(db, part)
		if p.ID != 0 {
			parsedCmd.TargetUser = p.Name
			parsedCmd.TargetUserID = p.ID
		}

		// We should make sure intV is always positive
		intV, err := strconv.Atoi(part)
		if err == nil {
			if intV < 0 {
				return &chat.ParsedCommand{}, parts, errors.New("You can't use negative amounts")
			}
			parsedCmd.TargetAmount = intV
		}

		// We have to make sure this is a props command
		if part == "all" {
			parsedCmd.TargetAmount = propsGiver.StreetCred
		}
	}

	return &parsedCmd, parts, nil
}

func ParseChatMessageForProps(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (error, *chat.ParsedCommand) {

	var parsedCmd chat.ParsedCommand
	parsedCmd.Username = msg.PlayerName
	parsedCmd.Streamgod = msg.Streamgod
	parsedCmd.Streamlord = msg.Streamlord

	propsGiver := player.Find(db, msg.PlayerName)

	parts := strings.Split(msg.Message, " ")

	name := string(parts[0])
	fc := string(name[0])
	if fc == "@" || fc == "!" {
		name = name[1:]
	}
	parsedCmd.Name = strings.ToLower(name)

	for _, part := range parts[1:] {
		fc := string(part[0])
		if fc == "@" || fc == "!" {
			part = part[1:]
		}
		part = strings.ToLower(part)

		c := stream_command.Find(db, part)
		if c.ID != 0 {
			parsedCmd.TargetCommand = c.Name
		}

		// We are checking the Street of the TargetUser
		altN := "송써니"
		if part == altN {
			part = "ssyuni"
		}
		p := player.Find(db, part)
		if p.ID != 0 {
			parsedCmd.TargetUser = p.Name
		}

		// We should make sure intV is always positive
		intV, err := strconv.Atoi(part)
		if err == nil {
			if intV < 0 {
				return errors.New("You can't use negative amounts"), &chat.ParsedCommand{}
			}
			parsedCmd.TargetAmount = intV
		}

		// We have to make sure this is a props command
		if part == "all" {
			parsedCmd.TargetAmount = propsGiver.StreetCred
		}
	}

	// We need to get the street cred somewhere else
	if propsGiver.StreetCred < parsedCmd.TargetAmount {
		msg := fmt.Sprintf("@%s doesn't have enough street cred %d/%d",
			parsedCmd.Username, parsedCmd.TargetAmount, msg.StreetCred)
		return errors.New(msg), &chat.ParsedCommand{}
	}

	return nil, &parsedCmd
}

func ParsePrivmsg(msg string) (string, string) {
	args := strings.Split(msg, " ")
	user := strings.Split(args[0], "!")[0][1:]
	user_msg := strings.Join(args[3:], " ")[1:]

	// We might more trimming here
	return user, strings.TrimSpace(user_msg)
}

func IsPrivmsg(msg string) bool {
	return strings.Contains(msg, "PRIVMSG")
}

func IsPing(msg string) bool {
	return strings.Contains(msg, "PING")
}

func IsCommand(msg string) bool {
	return string(msg[0]) == "!"
}

type ParsedCubeBet struct {
	Name      string
	Duration  int
	PlayerID  int
	SolveTime int
	Commands  []string
}

func ParseCubeBet(
	db *gorm.DB,
	msg *chat.ChatMessage,
) (*ParsedCubeBet, []string, []string, error) {
	var invalidCommands []string
	var unownedCommands []string

	if msg.PlayerID == 0 {
		return &ParsedCubeBet{}, invalidCommands, unownedCommands, errors.New(
			"No Player ID found")
	}

	parts := strings.Split(msg.Message, " ")

	fp := parts[0]
	if fp != "!bet" && fp != "!cubed" {
		return &ParsedCubeBet{}, invalidCommands, unownedCommands, nil
	}
	b := ParsedCubeBet{
		Name:     fp,
		PlayerID: msg.PlayerID,
	}

PartLoop:
	for _, part := range parts[1:] {

		intV, err := strconv.Atoi(part)
		if err == nil {
			if intV < 0 {
				return &ParsedCubeBet{}, invalidCommands, unownedCommands, errors.New(
					"You can't use negative amounts")
			}
			b.Duration = intV
			continue PartLoop
		}

		layout := "15:04:05"
		startTime, _ := time.Parse(layout, "00:00:00")
		parsedTime, err := time.Parse(layout, part)
		solveTime := time.Duration(parsedTime.UnixNano() - startTime.UnixNano())
		fmt.Printf("solveTime = %+v\n", solveTime)

		if err == nil {
			b.SolveTime = int(solveTime.Seconds())
			continue PartLoop
		}

		sc := stream_command.Find(db, part)
		if sc.ID == 0 {
			invalidCommands = append(invalidCommands, part)
			continue PartLoop
		}

		isAllowedToPlay := stream_command.IsAllowedToPlay(db, part, msg.PlayerID)
		if !isAllowedToPlay {
			unownedCommands = append(unownedCommands, part)
			continue PartLoop
		}

		fmt.Printf("Part Allowed to Play %+v\n", part)
		b.Commands = append(b.Commands, part)
	}

	if len(b.Commands) == 0 {
		commands := player.CommandsForPlayers(db, msg.PlayerID)
		b.Commands = commands
	}

	return &b, invalidCommands, unownedCommands, nil
}
