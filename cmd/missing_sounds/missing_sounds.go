package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"path/filepath"

	// Package image/jpeg is not used explicitly in the code below,
	// but is imported for its initialization side-effect, which allows
	// image.Decode to understand JPEG formatted images. Uncomment these
	// two lines to also understand GIF and PNG images:
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
)

func main() {
	db := database.CreateDBConn("beginsounds4")
	cmds := stream_command.All(db)
	for _, cmd := range cmds {
		potentialPlayer := player.Find(db, cmd.Name)
		if potentialPlayer.ID != 0 {
			fmt.Printf("\tTheme: %s\n", cmd.Name)
			tx := db.Model(&cmd).Update("theme", true)
			if tx.Error != nil {
				fmt.Printf("Error: %+v\n", tx.Error)
			}
		}
	}
}

func main2() {
	db := database.CreateDBConn("beginsounds4")

	memePath := "/home/begin/stream/Stream/ViewerImages/"
	// memePath := "/home/begin/stream/Stream/ViewerVideos/"

	// zubat.gif
	// imgFile, _ := os.Open("zubat.gif")
	// reader, _ := os.Open(memePath + "zubat.gif")

	files, _ := ioutil.ReadDir(memePath)
	for _, imgFile := range files {
		// imgFile := os.FileInfo

		if reader, err := os.Open(filepath.Join(memePath, imgFile.Name())); err == nil {
			defer reader.Close()
			filename := imgFile.Name()
			// filename := "zubat.gif"

			im, _, err := image.DecodeConfig(reader)

			ext := filepath.Ext(imgFile.Name())
			name := filename[0 : len(filename)-len(ext)]
			// m, _ := media_request.FindByName(db, name)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: %v\n", filename, err)
				// continue
			}

			// name := "zubat"
			// fmt.Printf("%s %d %d - %s | %s | %s\n\n", filename, im.Width, im.Height, name, ext, m.MediaType)
			meme, _ := memes.FindDefault(db, name)

			var newMeme memes.Meme
			if meme.ID == 0 {
				newMeme = memes.Meme{
					Name:         name,
					Width:        im.Width,
					Height:       im.Height,
					Scale:        1.0,
					MemeType:     "image",
					PositionType: "default",
					Filename:     filename,
				}
				tx := db.Create(&newMeme)
				if tx.Error != nil {
					fmt.Printf("tx.Error = %+v\n", tx.Error)
				}
			} else {
				// Update old Default
				newMeme = memes.Meme{
					Width:    im.Width,
					Height:   im.Height,
					MemeType: "image",
					Filename: filename,
				}
				db.Model(&meme).Updates(newMeme)
			}

			fmt.Printf("newMeme = %+v\n", newMeme)
			// fmt.Printf("MEME: %+v\n", meme)
			// Now we can lookup the meme, by the name and save the new info
			// I also need to look up the corresponding media request
			// fmt.Printf("%s %d %d %v\n", imgFile.Name(), im.Width, im.Height, im.ColorModel)
			// } else {
			// 	fmt.Println("Impossible to open the file:", err)
		}
	}
}
