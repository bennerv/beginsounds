package obs

import (
	"fmt"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
)

// SetSourcePosition - Set the position of a Source Position
func SetSourcePosition(
	client *OBSClient,
	sceneName string,
	sourceName string,
	x float64,
	y float64,
) error {

	req := obsws.NewSetSceneItemPositionRequest(sceneName, sourceName, x, y)
	_, err := processMoveSceneItem(client, &req)
	if err != nil {
		client.Logger.Println(err)
	}
	return err
}

// ToggleSource - allows toggling a source on or off
func ToggleSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	toggle bool,
) {
	req := obsws.NewSetSceneItemRenderRequest(sceneName, sourceName, toggle)
	client.execOBSCommand(&req)
}

// =============================================================

// RotateSource rotates the source
func RotateSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	rawRotation, ok := settings["rotation"]
	var rotation float64
	if ok {
		rotation = rawRotation.(float64)
	} else {
		rotation = 0.0
	}

	req := obsws.NewGetSceneItemPropertiesRequest(
		sceneName,
		nil,
		sourceName,
		0,
	)
	resp, err := client.execOBSCommand(&req)
	ogScene := resp.(obsws.GetSceneItemPropertiesResponse)

	req1 := obsws.NewSetSceneItemPropertiesRequest(
		sceneName,
		sourceName,
		sourceName, // itemName string,
		0,          // ID
		ogScene.PositionX,
		ogScene.PositionY,
		ogScene.PositionAlignment,
		// 0.0,
		float64(rotation),
		ogScene.ScaleX,
		ogScene.ScaleY,
		ogScene.CropTop,
		ogScene.CropBottom,
		ogScene.CropLeft,
		ogScene.CropRight,
		true,  // visible
		false, // locked
		ogScene.BoundsType,
		ogScene.BoundsAlignment,
		ogScene.BoundsX,
		ogScene.BoundsY)

	_, err = client.execOBSCommand(&req1)
	return err
}

// ===========================================================

func DefaultSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	ToggleSource(client, MemeScene, source, true)
	MoveSource(client, scene, source, settings)
	ScaleSource(client, scene, source, settings)
	RotateSource(client, scene, source, settings)
	return nil
}

func FlashSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {

	DefaultSource(client, scene, source, settings)
	<-time.NewTimer(5 * time.Second).C
	ToggleSource(client, MemeScene, source, false)
	return nil
}

func MoveSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {

	rawX, ok := settings["x"]
	if !ok {
		rawX = 800.0
	}
	x := rawX.(float64)

	rawY := settings["y"]
	if !ok {
		rawY = 400.0
	}
	y := rawY.(float64)

	fmt.Printf("Set Source Position %s %f %f\n", source, x, y)
	return SetSourcePosition(client, scene, source, x, y)
}

func HideSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	ToggleSource(client, scene, source, false)
	return nil
}

func ShowSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	rawToggle, ok := settings["toggle"]
	var sourceOn bool
	if !ok {
		sourceOn = true
	} else {
		sourceOn = rawToggle.(bool)
	}
	ToggleSource(client, scene, source, sourceOn)
	return nil
}

// UpdateSourceSettings update the source settings with a map
func UpdateSourceSettings(
	client *OBSClient,
	sourceName string,
	sourceType string,
	settings map[string]interface{},
) {
	req := obsws.NewSetSourceSettingsRequest(sourceName, sourceType, settings)
	client.execOBSCommand(&req)
}

// ScaleSource changes the scale of the source
func ScaleSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	rawScale, ok := settings["scale"]
	var scale float64
	if !ok {
		scale = 1.0
	} else {
		scale = rawScale.(float64)
	}

	rawRotation, ok := settings["rotation"]
	var rotation float64
	if !ok {
		rotation = 0.0
	} else {
		rotation = rawRotation.(float64)
	}

	req := obsws.NewSetSceneItemTransformRequest(
		sceneName,
		sourceName,
		scale,
		scale,
		rotation,
	)
	_, err := client.execOBSCommand(&req)
	return err
}
