package obs

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// UserEffects are effects related to actual Twitch Users/Other Streamers
func FilterEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	filterName := "greenscreen"
	if len(obsCmd.Parts) > 2 {
		filterName = obsCmd.Parts[2]
	}
	switch cmd {
	case "!greenscreen":
		fmt.Println("TRYING TO ADD GREEN SCREEN")
		AddGreenScreenFilter(client, obsCmd.Scene, obsCmd.Source)
	case "!filteron":
		ToggleFilter(client, obsCmd.Source, filterName, true)
	case "!filteroff":
		ToggleFilter(client, obsCmd.Source, filterName, false)
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

// UserEffects are effects related to actual Twitch Users/Other Streamers
func UserEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	switch cmd {
	case "!bg":
		BlackGlasses(client)
		ReturnToNormie(client)
	case "!obieru":
		sourceName := "obieru"
		ToggleSource(client, MemeScene, sourceName, true)
		max := 1000
		min := 200
		x := float64(rand.Intn(max-min) + min)
		obieruRising := map[string]interface{}{
			"start_y": 600.0,
			"end_y":   -500.0,
			"x":       x,
		}
		RiseSource(client, MemeScene, sourceName, obieruRising)
		ToggleSource(client, MemeScene, sourceName, false)
	case "!melkman2":
		ToggleSource(client, MemeScene, "melkman", true)
		SetSourcePosition(client, MemeScene, "melkman", 0, 0)
		ZoomAndSpin(client, "melkman", 6)
	case "!melkman":
		ToggleSource(client, MemeScene, "melkman", true)
		max := 1000
		min := 200
		x := float64(rand.Intn(max-min) + min)
		settings := map[string]interface{}{
			"start_y": 600.0,
			"end_y":   -500.0,
			"x":       x,
		}
		RiseSource(client, MemeScene, "melkman", settings)
		ToggleSource(client, MemeScene, "melkman", false)
	case "!primeagen":
		ToggleSource(client, MemeScene, "primeagen", true)
		sSettings := map[string]interface{}{
			"start_x": 0.0,
			"end_x":   1165.0,
			"y":       631.0,
		}
		SlideSource(client, MemeScene, "primeagen", sSettings)
	case "!roxkstar1":
		settings := map[string]interface{}{}
		ToggleSource(client, MemeScene, "roxkstar", true)
		BigBrain(client, "922", "roxkstar", settings)
		ToggleSource(client, MemeScene, "roxkstar", false)
	case "!teej1":
		settings := map[string]interface{}{}
		SlideInFromSide(client, MemeScene, "teejpopup", settings)
	case "!teej3":
		ToggleSource(client, MemeScene, "teejpopup", true)
		max := 500
		min := -100
		startX := 0.0
		endX := 275.0
		y := float64(rand.Intn(max-min) + min)
		// Should we pass these in like this???
		go func(
			c *OBSClient,
			start float64,
			end float64,
			yValue float64,
		) {
			// This min max is great for side teej
			settings := map[string]interface{}{
				"start_x": start,
				"end_x":   end,
				"y":       yValue,
			}
			SlideSource(c, obsCmd.Scene, "teejpopup", settings)
			<-time.NewTimer(time.Second * 1).C
			rSettings := map[string]interface{}{
				"start_x":  end,
				"end_x":    start,
				"y":        yValue,
				"modifier": -1.0,
			}
			ReverseSlideSource(c, obsCmd.Scene, "teejpopup", rSettings)
		}(client, startX, endX, y)
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

// ==================== //
// Begin Camera Effects //
// ==================== //
func BeginEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	switch cmd {
	case "!keeb":
		ToggleFilter(client, "primary", "KeyboardKeyboardPosition", true)
		ToggleFilter(client, "primary", "KeyboardBeginPosition", true)
		ToggleFilter(client, "primary", "KeyboardChatPosition", true)
	case "!risebegin":
		go RiseFromTheGrave(client)
	case "!pulse":
		sourceName := "922"
		ToggleFilter(client, sourceName, "outline", true)
		PulsatingHighlight(client, sourceName, 50, 6, false)
	case "!reaction":
		ReactionZoom(client, "922")
	case "!chadbegin":
		settings := map[string]interface{}{}
		go ChadSource(client, PrimaryScene, "922", settings)
	case "!zoombegin2":
		ToggleFilter(client, "922", "transform", true)
		ToggleFilter(client, "922", "outline", true)
		// False means don't turn this source off!
		go ZoomAndSpin2(client, "922", 10)
		go PulsatingHighlight(client, "922", 50, 10, false)
	case "!spazzbegin":
		ToggleFilter(client, "922", "transform", true)
		ToggleFilter(client, "922", "spazzbegin", true)
	case "!rollbegin":
		ToggleFilter(client, "922", "transform", true)
		ToggleFilter(client, "922", "rollbegin", true)
	case "!flipbegin":
		ToggleFilter(client, "922", "transform", true)
		ToggleFilter(client, "922", "flipbegin", true)
	case "!bigbrainbegin":
		ToggleFilter(client, "922", "transform", true)
		ToggleFilter(client, "922", "bigbrainbegin", true)
	case "!angrybegin":
		ToggleFilter(client, "922", "AngryBegin", true)
		ToggleFilter(client, "922", "BeginMad", true)
	case "!tallbegin":
		ToggleFilter(client, "922", "transform", true)
		TallBegin(client, "922", 30)
	case "!widebegin":
		ToggleFilter(client, "922", "transform", true)
		go func() {
			WideBegin(client, "922", 7)
		}()
	case "!highlight":
		ToggleFilter(client, "922", "outline", true)
		randomIndex := rand.Intn(len(colors))
		color, _ := strconv.ParseFloat(colors[randomIndex], 32)
		OutlineColor(client, color, "922")
	case "!color_green":
		color, _ := strconv.ParseFloat(Green, 32)
		OutlineColor(client, color, "922")
	case "!colorbrown":
		color, err := strconv.ParseFloat(Brown, 32)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}
		OutlineColor(client, color, "922")
	case "!hidebegin":
		go Hide(client)
	case "!intro":
		ChangeScene(client, "startin")
		ToggleFilter(client, "keyboard", "outline", true)
		ToggleSource(client, "startin", "keyboard", true)
		toggleTransform(client, "keyboard", true)
		PulsatingHighlight(client, "keyboard", 50, 30, false)
		// Rise2(client, PrimaryScene, "keyboard", map[string]interface{}{})
		ZoomAndSpin2(client, "keyboard", 8)
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessNormalizeRequests(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	switch cmd {
	case "!normie_transform", "!norm":
		parts := obsCmd.Parts
		// parts := strings.Split(msg.Message, " ")
		if len(parts) > 1 {
			source := parts[1]
			ToggleFilter(client, source, "transform", true)
			ToggleFilter(client, source, "normie_transform", true)
			ToggleFilter(client, source, "color_correction", false)
		} else {
			// We need to fix the camera color
			ToggleFilter(client, "922", "transform", true)
			ToggleFilter(client, "922", "normie_transform", true)
			ToggleFilter(client, "922", "color_correction", false)
		}
	case "!norm_all":
		sources, _ := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			// ToggleSource(c, mutex, MemeScene, source, true)
			go func(c1 *OBSClient, s string) {
				ToggleFilter(c1, s, "normie_transform", true)
				ToggleFilter(c1, s, "zoom_out", true)
				<-time.NewTimer(time.Millisecond * 200).C
				ToggleSource(c1, MemeScene, s, false)
			}(client, source)
			// <-time.NewTimer(time.Millisecond * 200).C
		}
	case "!nomeme":
		sources, _ := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			ToggleSource(client, MemeScene, source, false)
		}
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessHelpers(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	parts := obsCmd.Parts
	switch cmd {

	case "!find", "!reset":
		if len(parts) > 1 {
			source := parts[1]
			settings := map[string]interface{}{
				"scale": 1.0,
			}
			SetSourcePosition(client, MemeScene, source, 0.0, 0.0)
			ScaleSource(client, MemeScene, source, settings)
			ToggleSource(client, MemeScene, source, true)
			ToggleFilter(client, source, "color_correction", false)
			ToggleFilter(client, source, "transform", true)
			ToggleFilter(client, source, "normie_transform", true)
		}
	case "!idk":
		var name string
		if len(parts) < 2 {
			name = "test1"
		} else {
			name = parts[1]
		}
		ms := []memes.Meme{}
		sources, _ := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			meme := MemeInfo(client, source)
			ms = append(ms, meme)
		}
		memeFile, err := os.Create(fmt.Sprintf("assets/ViewerSceneJson/%s.json", name))
		if err != nil {
			fmt.Printf("\nError Creating Built JSON: %s", err)
		}
		encoder := json.NewEncoder(memeFile)
		encoder.Encode(ms)
		memeFile.Close()

		for _, meme := range ms {
			scaleSettings := map[string]interface{}{
				"scale": meme.Scale,
			}
			ScaleSource(client, MemeScene, meme.Name, scaleSettings)
			x, y := RandomPos()
			moveSettings := map[string]interface{}{
				"x": x,
				"y": y,
			}
			MoveSource(client, MemeScene, meme.Name, moveSettings)

			// MoveSource2(c, mutex, MemeScene, meme.Name, moveSettings)
			// ToggleSource(c, mutex, MemeScene, meme.Name, true)
			// ToggleFilter(c, mutex, meme.Name, "color_correction", false)
			// ToggleFilter(c, mutex, meme.Name, "transform", true)
			// ToggleFilter(c, mutex, meme.Name, "normie_transform", true)
		}
	case "!images":
		images := ImageSources(client)
		results <- images
	case "!filtersettings":
		res := FetchFilterSettings(client, "922", "transform")
		results <- fmt.Sprintf("%v", res)
	case "!settings":
		FetchSourceSettings(client, "sealspin", "ffmpeg_source")
	// case "!clone":
	// 	if len(parts) > 1 {
	// 		source := parts[1]
	// 		fmt.Println("IT'S CLONING TIME: ", source)
	// 		CloneThang(client, MemeScene, source, 5)
	// 	}
	case "!addfilters":
		if len(parts) > 1 {
			source := parts[1]
			// AddOutlineFilter(c, mutex, MemeScene, source)
			// AddTransformFilter(c, mutex, MemeScene, source)
			AddNormieTransformFilter(client, MemeScene, source)
			AddColorCorrectionFilter(client, MemeScene, source)
			AddColorFadeFilter(client, MemeScene, source)
			AddOutlineFilter(client, MemeScene, source)
			AddTransformFilter(client, MemeScene, source)
		}
	case "!fixnorm":
		if len(parts) > 1 {
			source := parts[1]
			UpdateNormieTransform(client, MemeScene, source)
		}
	case "!memes":
		fmt.Println("LOOKING ")
		// I could iterate through every meme
		// and call stats on it,
		// and save those stats to a file
		// I then could create, just call
		// each command on its own
		sources, _ := SourcesByVisibility(client, MemeScene)
		results <- fmt.Sprintf("Memes: %s", strings.Join(sources, ", "))
		results <- "https://beginworld.website-us-east-1.linodeobjects.com/memes.html"
		website_generator.SyncMemesPage()
	case "!hiddenmemes":
		results <- "http://www.beginworld.exchange/memes.html"
		website_generator.SyncPage("memes")
	case "!stats":
		if len(parts) > 1 {
			source := parts[1]
			CreateFakeSource(client)
			meme := MemeInfo(client, source)
			results <- fmt.Sprintf("Source: %s | X: %f Y: %f", source, meme.X, meme.Y)
		}
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessEpicEffects(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {

	effectTriggered := true
	var err error

	// !clippy Begin use an interface
	// That would result in Clippy rising
	// with a message saying:
	//   "Hey have you thought of: Begin us an interface"
	switch cmd {
	case "!iasip", "!sunny":
		msg := strings.Join(obsCmd.Parts[1:], " ")
		// website_generator.CreateSunnyPage(client.DB, "The Gang Chooses a Linux Distro")
		ToggleSource(client, "iasip", "SunnyHtml", false)
		website_generator.CreateSunnyPage(client.DB, msg)
		ToggleSource(client, "iasip", "SunnyHtml", true)
		ChangeScene(client, "iasip")
		go func() {
			<-time.NewTimer(5 * time.Second).C
			ChangeScene(client, "primary")
		}()
	case "!clippy":
		ToggleSource(client, MemeScene, "clippy", true)
		ToggleSource(client, MemeScene, "clippytextboxhd", true)
		clippyRiseSettings := map[string]interface{}{
			"start_y": -400.0,
			"end_y":   425.0,
			"x":       700.0,
		}
		clippyTextRiseSettings := map[string]interface{}{
			"start_y": -400.0,
			"end_y":   300.0,
			"x":       575.0,
		}
		RiseSource(client, MemeScene, "clippy", clippyRiseSettings)
		RiseSource(client, MemeScene, "clippytextboxhd", clippyTextRiseSettings)
		ToggleSource(client, MemeScene, "ClippyAdvice", true)
	case "!demo":
		// if msg.Streamgod {
		_, sources := SourcesByVisibility(client, MemeScene)
		for _, _ = range sources {
			// ToggleSource(c, mutex, MemeScene, source, true)
			// f := randomTransformFunc()
			// go f(c, mutex, MemeScene, source)
			// <-time.NewTimer(time.Millisecond * 200).C
		}
		// }
	case "!credits":
		// if msg.Streamgod {
		_, sources := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			ToggleSource(client, MemeScene, source, true)
			settings := map[string]interface{}{
				"rotation": 0.0,
			}
			RotateSource(client, MemeScene, source, settings)
			SetSourcePosition(client, MemeScene, source, 0.0, 100.0)
			ToggleFilter(client, source, "normie_transform", true)
			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1450.0,
				"y":       100.0,
			}
			SlideSource(client, MemeScene, source, slideSettings)
			ToggleSource(client, MemeScene, source, false)
		}
		// }
	case "!allmeme":
		// if msg.Streamgod {
		_, sources := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			ToggleSource(client, MemeScene, source, true)
		}
		// }
	case "!danceparty":
		sources := []string{"vibecat", "puppy", "technofroggo", "sealspin"}
		for _, source := range sources {
			// Make em wacky!
			ToggleFilter(client, source, "transform", true)
			FunZoom(client, source)
			// Turn it on
			ToggleSource(client, MemeScene, source, true)
			// Make it normal
			ToggleFilter(client, source, "normie_transform", true)
		}
		SetSourcePosition(client, MemeScene, "technofroggo", 1275.0, -1.0)
		SetSourcePosition(client, MemeScene, "puppy", 0.0, -5.0)
		SetSourcePosition(client, MemeScene, "vibecat", -12.0, 415)
		SetSourcePosition(client, MemeScene, "sealspin", 850, 300)
	case "!dealwithitboys":
		// Source: dealwithit | X: 943.000000 Y: 433.194702
		// Source: blunt | X: 905.000000 Y: 529.000000
		scene := MemeScene
		for _, source := range []string{"dealwithit", "blunt"} {
			FunZoom(client, source)
			ToggleSource(client, scene, source, true)
			ToggleFilter(client, source, "transform", true)
			ToggleFilter(client, source, "normie_transform", true)
		}
	case "!dealwithit":
		scene := MemeScene
		// sunglassess := MemeInfo(c, mutex, "dealwithit")
		// blunt := MemeInfo(c, mutex, "blunt")
		for _, source := range []string{"dealwithit", "blunt"} {
			ToggleSource(client, scene, source, true)
		}
		// Source: dealwithit | X: 857.000000 Y: 432.000000
		//  Source: blunt | X: 842.000000 Y: 521.000000
		modifier := 1.5
		go Fall(
			client,
			scene,
			"dealwithit",
			875.0,
			0,
			432.0,
			modifier,
		)
		// <-time.NewTimer(time.Millisecond * 200).C
		go Slide(client, scene, "blunt", 300, 842.0, 521.0, modifier)
	case "!pulseseal":
		ToggleSource(client, MemeScene, "sealspin", true)
		ToggleFilter(client, "sealspin", "transform", true)
		ToggleFilter(client, "sealspin", "outline", true)
		PulsatingHighlight(client, "sealspin", 50, 7, true)
		ZoomAndSpin(client, "sealspin", 5)
		// player.UpdateMana(db, *msg.PlayerID, uint(mana-10))

	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func ProcessCreateSourcesRequests(
	client *OBSClient,
	db *gorm.DB,
	cmd string,
	obsCmd Command,
	results <-chan string,
) (bool, error) {

	effectTriggered := true
	var err error

	msgBreakdown := obsCmd.Parts
	switch cmd {
	case "!defaults":
		m := []memes.Meme{}
		tx := db.Model(&memes.Meme{}).Find(&m)
		if tx.Error != nil {
			fmt.Printf("tx.Error = %+v\n", tx.Error)
		}

		// settings := map[string]interface{}{}
		for _, meme := range m {
			settings := map[string]interface{}{
				"x":       meme.X,
				"start_x": 0.0,
				// "start_x":  meme.X,
				"end_x":    meme.X,
				"y":        meme.Y,
				"start_y":  meme.Y,
				"end_y":    meme.Y,
				"rotation": meme.Rotation,
				"scale":    meme.Scale,
			}
			// if meme.X < 400.0 {
			// 	settings["start_x"] = 1200.0
			// } else {
			// 	settings["start_x"] = 0.0
			// }
			SetSourcePosition(client, MemeScene, meme.Name, 0.0, 0.0)
			SlideSource(client, MemeScene, meme.Name, settings)
		}

	case "!save":

		var name string
		if len(obsCmd.Parts) > 1 {
			name = obsCmd.Parts[1]
		}

		fmt.Printf("\n\tAttempting to Save: %+v | Parts: %v\n", name, obsCmd.Parts)

		// CreateFakeSource(client)

		if name != "" {
			meme := MemeInfo(client, name)

			fmt.Printf("\t+++ Meme: %+v", meme)

			err := meme.Save(db)
			if err == nil {
				fmt.Println(fmt.Sprintf("Saving Position of: %s", name))
				// results <- fmt.Sprintf("Saving Positition of: %s", name)
			} else {
				fmt.Println(fmt.Sprintf("Error Saving Position of: %v", err))
			}
		}
	case "!create_image_source":
		if len(msgBreakdown) > 1 {
			imageName := msgBreakdown[1]
			fmt.Printf("\t -- We are going to try and Create a new image: %s", imageName)
			CreateAndPrepareImageSource(client, db, MemeScene, imageName)
			settings := map[string]interface{}{
				"scale": 0.5,
			}
			ScaleSource(client, obsCmd.Scene, imageName, settings)

			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1365.0,
				"y":       300.0,
			}
			SlideSource(client, obsCmd.Scene, imageName, slideSettings)
		}
	case "!create_video_source":
		if len(msgBreakdown) > 1 {
			videoName := msgBreakdown[1]
			CreateAndPrepareVideoSource(client, db, MemeScene, videoName)
			// CreateAndPrepareImageSource(c, mutex, db, videoName)
			settings := map[string]interface{}{
				"scale": 0.5,
			}
			ScaleSource(client, MemeScene, videoName, settings)

			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1365.0,
				"y":       300.0,
			}
			SlideSource(client, MemeScene, videoName, slideSettings)
		}
	default:
		effectTriggered = false
	}

	return effectTriggered, err
}

func RandomRouter(
	client *OBSClient,
	cmd string,
	obsCmd Command,
	results chan<- string,
) (bool, error) {
	switch cmd {

	case "!zoom_out":
		// This probably needs  error handling?
		settings := map[string]interface{}{}
		ToggleFilter(client, obsCmd.Source, "zoom_out", true)
		BigBrain(client, obsCmd.Scene, obsCmd.Source, settings)
	case "!zs":
		if len(obsCmd.Parts) > 1 {
			ZoomSpinSource(
				client,
				obsCmd.Scene,
				obsCmd.Source,
				obsCmd.Settings,
			)
		}

	case "!random":
		if obsCmd.Scene != "" && obsCmd.Source != "" {
			// obsCmd.Settings is uninitialized???
			f := randomTransformFunc()
			f(
				client,
				obsCmd.Scene,
				obsCmd.Source,
				obsCmd.Settings,
			)
		}

	// ==================== //
	// Toggle and Transform //
	// ==================== //
	case "!alerts":
		sourceName := "Alerts"
		ToggleFilter(client, sourceName, "transform", true)
		ZoomAndSpin(client, sourceName, 5)
	// Theory: We aren't getting
	// effects lower in this list
	case "!beginworld":
		ToggleSource(client, MemeScene, "beginworld", true)
		toggleTransform(client, "beginworld", true)
		ZoomAndSpin(client, "beginworld", 8)
	case "!seal":
		ToggleSource(client, MemeScene, "sealspin", true)
	case "!puppy":
		ToggleSource(client, MemeScene, "puppy", true)
		var duration time.Duration
		duration = 30
		DelaySpin(client, MemeScene, "puppy", 2, duration)
	case "!zoomseal":
		zoomseal(client)
	case "!otherspin":
		ToggleFilter(client, "922", "transform", true)
		ZoomAndSpin(client, "922", 7)
	case "!spinseal":
		ToggleSource(client, MemeScene, "sealspin", true)
		ToggleFilter(client, "sealspin", "transform", true)
		ZoomAndSpin(client, "922", 5)
	case "!beginboy":
		beginboy(client)

	// ============= //
	// Toggle Scenes //
	// ============= //
	case "!hateflow":
		ChangeScene(client, "emperor")

	case "!cube_news":
		go func() {
			ChangeScene(client, "breakin")
			<-time.NewTimer(time.Second * 6).C
			ChangeScene(client, "news")
		}()

	}

	// TODO: Update passing to charge
	return false, nil
}
