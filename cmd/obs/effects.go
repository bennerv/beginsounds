package obs

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

// Effects //
// Effects will use lower-level transformations
// together to produce an effect
// ======= //
// Sliding //
// ======= //

// SlideSource - Slides the source across the screen
func SlideSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {

	ToggleSource(client, MemeScene, source, false)

	// rawScale, _ := settings["scale"]
	// scale := rawScale.(float64)
	ScaleSource(client, scene, source, settings)

	rawStartX, _ := settings["start_x"]
	rawEndX, _ := settings["end_x"]
	rawY, _ := settings["y"]
	var rawModifier interface{}
	rawModifier, ok := settings["modifier"]
	if !ok {
		rawModifier = 0.8
	}
	modifier := rawModifier.(float64)
	startX := rawStartX.(float64)
	endX := rawEndX.(float64)
	y := rawY.(float64)
	moveSettings := map[string]interface{}{
		"x": startX,

		"y": y,
	}

	MoveSource(client, scene, source, moveSettings)
	RotateSource(client, scene, source, settings)
	ToggleFilter(client, source, "color_correction", false)
	ToggleFilter(client, source, "transform", true)
	ToggleFilter(client, source, "normie_transform", true)
	ToggleSource(client, MemeScene, source, true)

	fmt.Printf("\tSliding Source: %s source, settings = %+v\n", source, settings)

	if startX < endX {
		for x := startX; x < endX; x += modifier {
			SetSourcePosition(client, scene, source, x, y)
		}
	} else {
		for x := startX; x > endX; x -= modifier {
			SetSourcePosition(client, scene, source, x, y)
		}
	}

	return nil
}

func SlideOutSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	rawStartX, _ := settings["start_x"]
	rawEndX, _ := settings["end_x"]
	rawY, _ := settings["y"]

	var rawModifier interface{}
	rawModifier, ok := settings["modifier"]
	if !ok {
		rawModifier = 0.8
	}
	modifier := rawModifier.(float64)

	startX := rawStartX.(float64)
	endX := rawEndX.(float64)
	y := rawY.(float64)

	for x := startX; x > endX; x -= modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}

	return nil
}

func Slide(
	client *OBSClient,
	sceneName string,
	sourceName string,
	startX float64,
	endX float64,
	y float64,
	modifier float64,
) {
	for x := startX; x < endX; x += modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}
}

// SlideInFromSide peaks out a source from the side
func SlideInFromSide(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	rand.Seed(time.Now().UnixNano())

	ToggleSource(client, sceneName, sourceName, true)
	settings["rotation"] = 90.0
	RotateSource(client, sceneName, sourceName, settings)

	startX := 0.0
	endX := 1275.0
	y := 400.0
	settings = map[string]interface{}{
		"start_x": startX,
		"end_x":   endX,
		"y":       y,
	}
	SlideSource(client, sceneName, sourceName, settings)
	<-time.NewTimer(time.Second * 1).C
	reverseSettings := map[string]interface{}{
		"start_x":  endX,
		"end_x":    startX,
		"y":        y,
		"modifier": -1.0,
	}
	ReverseSlideSource(client, sceneName, sourceName, reverseSettings)
	return nil
}

// ===============================================

// Spin spins the specified source for the delay passed in
func Spin(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},

) error {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	delay, _ := settings["delay"]
	d := time.Duration(delay.(int))
	timer := time.NewTimer(time.Second * d)

	toggleTransform(client, sourceName, true)
	x := 0
	for {
		select {
		case <-timer.C:
			return nil
		case _ = <-ticker.C:

			filterSettings := map[string]interface{}{
				"Filter.Transform.Scale.X":    x,
				"Filter.Transform.Scale.Y":    x,
				"Filter.Transform.Rotation.X": x,
				"Filter.Transform.Rotation.Y": x,
				"Filter.Transform.Rotation.Z": x,
			}

			setSourceFilterSettings(client, sourceName, filterName, filterSettings)
			x++
		}
	}
}

// Color =============================================

func FadeToColor(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	color, ok := settings["color"].(float64)
	if ok {
		SetColorFade(client, sourceName, "colorfade", color)
		ToggleFilter(client, sourceName, "colorfade", true)
		ToggleFilter(client, sourceName, "color_correction", true)
	}
	return nil
}

// ===========================================================================

// This is just some settings and a filter
func FunZoom(
	client *OBSClient,
	sourceName string,
) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera":             0,
		"Filter.Transform.Camera.FieldOfView": 90,
		"Filter.Transform.Position.X":         0,
		"Filter.Transform.Position.Y":         -0.03,
		"Filter.Transform.Position.Z":         0,
		"Filter.Transform.Rotation.X":         -180,
		"Filter.Transform.Rotation.Y":         180,
		"Filter.Transform.Rotation.Z":         0,
		"Filter.Transform.Scale.X":            0,
		"Filter.Transform.Scale.Y":            0,
		"Filter.Transform.Shear.X":            200,
		"Filter.Transform.Shear.Y":            200,
	}
	filterName := "transform"
	setSourceFilterSettings(client, sourceName, filterName, filterSettings)
}

// ===============================================================================

func floatSourceThroughScene(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	ToggleSource(client, sourceName, sceneName, true)
	RotateSource(client, sceneName, sourceName, settings)

	rand.Seed(time.Now().UnixNano())
	max := 1400
	min := 200
	x := float64(rand.Intn(max-min) + min)
	startY := 600.0
	endY := -600.0

	riseSettings := map[string]interface{}{
		"x":       x,
		"start_y": startY,
		"end_y":   endY,
	}
	RiseSource(client, sceneName, sourceName, riseSettings)
	return nil
}

func AppearFromThinAir(client *OBSClient, sourceName string) {
	filterName := "transform"
	ToggleFilter(client, sourceName, "transform", true)

	start := -90.0
	end := 0.
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.01 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     0,
			"Filter.Transform.Rotation.X": i,
		}
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

func ZoomFieldOfView(client *OBSClient, sourceName string) {
	filterName := "transform"
	ToggleFilter(client, sourceName, "transform", true)

	start := 179.0
	end := 75.0
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.02 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": i,
		}
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

// ReverseSlideSource is used to reverse a slide
func ReverseSlideSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) {

	rawStartX, _ := settings["start_x"]
	rawEndX, _ := settings["end_x"]
	rawY, _ := settings["y"]
	rawModifier, _ := settings["modifier"]

	startX := rawStartX.(float64)
	endX := rawEndX.(float64)
	y := rawY.(float64)
	modifier := rawModifier.(float64)

	for x := startX; x > endX; x += modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}

}

func Fall(
	client *OBSClient,
	sceneName string,
	sourceName string,
	x float64,
	startY float64,
	endY float64,
	modifier float64,
) {
	for y := startY; y < endY; y += modifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}
}

// FallSource - moves source from current Y position to default Y position
func FallSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {

	fmt.Println("\nATTEMPTING TO FALL")

	// RotateSource(client, scene, source, settings)
	// ToggleFilter(client, source, "color_correction", false)
	// ToggleFilter(client, source, "transform", true)
	// ToggleFilter(client, source, "normie_transform", true)
	ToggleSource(client, MemeScene, source, true)

	rawModifier := settings["modifier"]
	rawX := settings["x"]
	rawStartY := settings["start_y"]
	rawEndY := settings["end_y"]

	modifier := rawModifier.(float64)
	x := rawX.(float64)
	startY := rawStartY.(float64)
	endY := rawEndY.(float64)

	Fall(client, scene, source, x, startY, endY, modifier)

	return nil
}

func RiseSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	YModifier := 0.8

	startY := settings["start_y"].(float64)
	endY := settings["end_y"].(float64)
	x := settings["x"].(float64)

	for y := startY; y > endY; y -= YModifier {
		SetSourcePosition(client, sceneName, sourceName, x, y)
	}
	return nil
}

func ZoomAndSpin3(
	client *OBSClient,
	sourceName string,
	delay time.Duration,
) {

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	go func() {
		x := 0.0
		y := 0.0
		z := 0.0
		xModifier := -0.5
		yModifier := 0.8
		zModifier := -0.4

		for {
			select {
			case <-timer.C:
				return
			case t := <-ticker.C:
				fmt.Println("t: ", t)

				// Cool Weird slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    y,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": y,
					"Filter.Transform.Rotation.Z": z,
				}
				setSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x = x + xModifier
				y = y + yModifier
				z = z + zModifier
			}
		}
	}()
}

func ZoomAndSpin2(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	go func() {
		x := 0.0
		y := 0.0
		z := 0.0
		modifier := 0.7
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				// Cool Weird slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    y,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": y,
					"Filter.Transform.Rotation.Z": z,
				}

				setSourceFilterSettings(
					client,
					sourceName,
					filterName,
					filterSettings,
				)
				x = x + modifier
				y = y - modifier
				z = z + (modifier + 0.3)
			}
		}
	}()
}

func ZoomAndSpin(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * delay)

	go func() {
		x := 0
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				// Cool Wierd slow zoom in
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    x,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": x,
					"Filter.Transform.Rotation.Z": x,
				}

				setSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x++
			}
		}
	}()

}

func TallSource(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 20)

	go func() {
		y := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.Y": y,
				}
				setSourceFilterSettings(client, source, filterName, filterSettings)
				y = y + 4
			}
		}
	}()

	return nil
}

func WideSource(
	client *OBSClient,
	scene string, // Scene
	source string, // Source
	settings map[string]interface{}, // Settings
) error {

	rawDuration, ok := settings["duration"]
	var duration time.Duration
	if ok {
		duration = rawDuration.(time.Duration)
	} else {
		duration = 7
	}

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * duration)

	go func() {
		x := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
				}
				setSourceFilterSettings(client, source, filterName, filterSettings)
				x = x + 4
			}
		}
	}()

	return nil

}

func WideBegin(
	client *OBSClient,
	sourceName string,
	duration time.Duration,
) {

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * duration)

	go func() {
		x := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
				}
				setSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x = x + 4
			}
		}
	}()

}

func TallBegin(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 20)

	go func() {
		y := 1

		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.Y": y,
				}
				setSourceFilterSettings(client, sourceName, filterName, filterSettings)
				y = y + 4
			}
		}
	}()

}

func TheZoom(client *OBSClient, sourceName string, delay time.Duration) {
	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 7)

	go func() {
		x := 0
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				// x := rand.Intn(max-min) + min
				// y := rand.Intn(max-min) + min
				// z := rand.Intn(max-min) + min
				// y := x / 2
				// z := x / 2
				// filterSettings := map[string]interface{}{"Filter.Transform.Rotation.X": x}
				// filterSettings := map[string]interface{}{
				// 	"Filter.Transform.Rotation.X": x,
				// 	"Filter.Transform.Rotation.Y": x,
				// 	"Filter.Transform.Rotation.Z": x,
				// 	// "Filter.Transform.Rotation.Y": y,
				// 	// "Filter.Transform.Rotation.Z": z,
				// }
				// This scaling up is a zoom in a affect
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X": x,
					"Filter.Transform.Scale.Y": x,
				}

				// Zoom in Spin thang
				// y := rand.Intn(max-min) + min
				// z := rand.Intn(max-min) + min
				// filterSettings := map[string]interface{}{
				// 	"Filter.Transform.Scale.X":    x,
				// 	"Filter.Transform.Scale.Y":    x,
				// 	"Filter.Transform.Rotation.X": x,
				// 	"Filter.Transform.Rotation.Y": y,
				// 	"Filter.Transform.Rotation.Z": z,
				// }

				// Cool Wierd slow zoom in
				// filterSettings := map[string]interface{}{
				// 	"Filter.Transform.Scale.X":    x,
				// 	"Filter.Transform.Scale.Y":    x,
				// 	"Filter.Transform.Rotation.X": x,
				// 	"Filter.Transform.Rotation.Y": x,
				// 	"Filter.Transform.Rotation.Z": x,
				// }
				setSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x++
			}
		}
	}()
}

// This is a Ticker Pattern
// We increase some values according to a pattern
// and then stop based on a Timer
func DelaySpin(
	client *OBSClient,
	sceneName string, sourceName string,
	delay time.Duration,
	duration time.Duration,
) {

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)

	go func() {
		delayStartTimer := time.NewTimer(time.Second * delay)
		<-delayStartTimer.C
		timer := time.NewTimer(time.Second * duration)

		x := 50
		for {
			select {
			case <-timer.C:
				return
			case _ = <-ticker.C:
				filterSettings := map[string]interface{}{
					"Filter.Transform.Scale.X":    x,
					"Filter.Transform.Scale.Y":    x,
					"Filter.Transform.Rotation.X": x,
					"Filter.Transform.Rotation.Y": x,
					"Filter.Transform.Rotation.Z": x,
				}
				setSourceFilterSettings(client, sourceName, filterName, filterSettings)
				x++
			}
		}
	}()
}

// BigBrain gives the source a bigger brain...duh
func BigBrain(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	filterName := "transform"
	ToggleFilter(client, sourceName, filterName, true)

	start := 0.00
	end := -50.5
	ticker := time.NewTicker(5 * time.Millisecond)

	for i := start; i > end; i -= 0.01 {
		_ = FetchFilterSettings(client, sourceName, "transform")

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     1,
			"Filter.Transform.Position.Y": 0,
			"Filter.Transform.Position.Z": 0.00,
			"Filter.Transform.Rotation.X": i,
			"Filter.Transform.Rotation.Y": 0,
			"Filter.Transform.Rotation.Z": 0,
			"Filter.Transform.Scale.Y":    110,
		}
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
	return nil
}

func ChadSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	filterName := "transform"
	ToggleFilter(client, sourceName, filterName, true)

	start := 0.00
	end := 259.26

	for i := start; i < end; i += 0.03 {
		_ = FetchFilterSettings(client, sourceName, filterName)

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":     1,
			"Filter.Transform.Position.Y": -0.12999999999999998,
			"Filter.Transform.Rotation.X": 58.65,
			"Filter.Transform.Rotation.Y": 3.03,
			"Filter.Transform.Rotation.Z": 0,
			"Filter.Transform.Scale.X":    111.11,
			"Filter.Transform.Scale.Y":    i,
		}
		sourceName := sourceName
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}

	return nil
}

func BlackGlasses(client *OBSClient) {
	ToggleSource(client, PrimaryScene, "i_need_attention", true)
	ToggleFilter(client, "i_need_attention", "transform", true)

	start := -50.00
	end := 32.61
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i < end; i += 0.01 {
		_ = FetchFilterSettings(client, "i_need_attention", "transform")

		filterSettings := map[string]interface{}{
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": i,
			"Filter.Transform.Position.X":         -i,
			"Filter.Transform.Position.Y":         10,
		}
		sourceName := "i_need_attention"
		filterName := "transform"
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

func Zoom(
	client *OBSClient,
	scene string,
	source string,
	settings map[string]interface{},
) error {
	rl, _ := settings["level"]
	level := rl.(float64)
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera":             1,
		"Filter.Transform.Camera.FieldOfView": level,
	}
	filterName := "transform"
	setSourceFilterSettings(client, source, filterName, filterSettings)
	return nil
}

func ReactionZoom(client *OBSClient, source string) {
	ToggleFilter(client, source, "zoom", true)

	start := 100.0
	end := 20.0
	ticker := time.NewTicker(2 * time.Second)

	for i := start; i > end; i -= 20 {
		settings := map[string]interface{}{
			"level": i,
		}
		Zoom(client, MemeScene, source, settings)
		<-ticker.C
	}
}

func RiseFromTheGrave(client *OBSClient) {
	// ToggleSource(c, mutex, PrimaryScene, "i_need_attention", true)
	ToggleFilter(client, "922", "transform", true)

	start := 175.0
	end := 0.0
	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.005 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
		}
		sourceName := "922"
		filterName := "transform"
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C
}

// ZoomSpinSource takes a source and zooms and spins it
// 	until the duration
func ZoomSpinSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	filterName := "transform"
	ticker := time.NewTicker(30 * time.Millisecond)
	timer := time.NewTimer(time.Second * 5)

	x := 0
	for {
		select {
		case <-timer.C:
			return nil
		case _ = <-ticker.C:
			filterSettings := map[string]interface{}{
				"Filter.Transform.Scale.X":    x,
				"Filter.Transform.Scale.Y":    x,
				"Filter.Transform.Rotation.X": x,
				"Filter.Transform.Rotation.Y": x,
				"Filter.Transform.Rotation.Z": x,
			}
			setSourceFilterSettings(client, sourceName, filterName, filterSettings)
			x++
		}
	}

	return nil
}

func ZoomOut(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {
	ToggleFilter(client, sourceName, "zoom_out", true)
	// BigBrain(c, mutex, sceneName, sourceName, settings)
	return nil
}

// UnveilSource rises the source
func UnveilSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	settings map[string]interface{},
) error {

	filterName := "transform"
	ToggleFilter(client, sourceName, filterName, true)

	// start := 175.0
	// end := 0.0
	rawStartY := settings["start_y"]
	rawEndY := settings["end_y"]
	start := rawStartY.(float64)
	end := rawEndY.(float64)

	ticker := time.NewTicker(100 * time.Millisecond)

	for i := start; i > end; i -= 0.010 {
		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
		}
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
	<-ticker.C

	return nil
}

// =============== //
// Color / Outling //
// =============== //

func SuperSaiyan(client *OBSClient) {
	// go BigBrain(c, mutex)
	PulsatingHighlight(client, "922", 50, 60, true)
	RiseFromTheGrave(client)
}

func PulsatingHighlight(
	client *OBSClient,
	sourceName string,
	delay time.Duration,
	duration time.Duration,
	toggleOff bool,
) {

	timer := time.NewTimer(duration * time.Second)

	go func() {
		for {
			select {
			case <-timer.C:
				ReturnToNormie(client)
				if toggleOff {
					ToggleSource(client, PrimaryScene, sourceName, false)
				}
				return
			default:
				for _, colorFloat := range colors {
					color, err := strconv.ParseFloat(colorFloat, 32)
					if err != nil {
						client.Logger.Printf("err = %+v\n", err)
					}
					// This might be overkill
					OutlineColor(client, color, sourceName)
					time.Sleep(time.Millisecond * delay)
				}
			}
		}
	}()

}

// =============================== BORING

func Hide(client *OBSClient) {
	ToggleFilter(client, "922", "transform", true)

	start := 100.0
	end := 150.0

	for i := start; i < end; i += 0.005 {

		filterSettings := map[string]interface{}{
			"Filter.Transform.Position.Y": i,
		}
		sourceName := "922"
		filterName := "transform"
		setSourceFilterSettings(client, sourceName, filterName, filterSettings)
	}
}
