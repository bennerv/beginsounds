package obs

import (
	"fmt"
	"math/rand"
	"path/filepath"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gorm.io/gorm"
)

// CreateAndPrepareImageSource creates an image source,
// 	adds filters and moves the image
func CreateAndPrepareImageSource(
	client *OBSClient,
	db *gorm.DB,
	sceneName string,
	imageName string,
) {
	mr, err := media_request.FindByName(db, imageName)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	ext := filepath.Ext(mr.Filename)
	filename := fmt.Sprintf("/home/begin/stream/Stream/ViewerImages/%s%s", mr.Name, ext)

	CreateImageSource(client, sceneName, mr.Name, filename)
	db.Model(&mr).Update("deleted_at", time.Now())
	AddOutlineFilter(client, sceneName, imageName)
	AddTransformFilter(client, sceneName, imageName)
	AddNormieTransformFilter(client, sceneName, imageName)
	ToggleFilter(client, imageName, "color_correction", false)
}

// CreateAndPrepareVideoSource create a FFMPEG source
//  with all the filters
func CreateAndPrepareVideoSource(
	client *OBSClient,
	db *gorm.DB,
	sceneName string,
	videoName string,
) {

	mr, err := media_request.FindByName(db, videoName)
	if err != nil {
		client.Logger.Printf("Cre")
		fmt.Printf("err = %+v\n", err)
		return
	}

	// I need the file extension here
	filename := fmt.Sprintf("/home/begin/stream/Stream/ViewerVideos/%s.gif", mr.Name)
	fmt.Printf("filename = %+v\n", filename)

	CreateMediaSource(client, sceneName, mr.Name, filename)
	db.Model(&mr).Update("deleted_at", time.Now())
	AddOutlineFilter(client, sceneName, videoName)
	AddTransformFilter(client, sceneName, videoName)
	AddNormieTransformFilter(client, sceneName, videoName)
	AddColorCorrectionFilter(client, MemeScene, videoName)
	AddColorFadeFilter(client, MemeScene, videoName)
	ToggleFilter(client, videoName, "color_correction", false)
}

// CreateImageSource create a new image on the codin source
func CreateImageSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	filename string,
) error {
	sourceKind := "image_source"
	settings := map[string]interface{}{
		"file": filename,
	}
	return CreateSource(client, sceneName, sourceName, sourceKind, settings)
}

// CreateMediaSource creates a ffmpeg source in OBS
func CreateMediaSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	filename string,
) error {
	sourceKind := "ffmpeg_source"

	settings := map[string]interface{}{
		"local_file":          filename,
		"looping":             true,
		"Render":              true,
		"close_when_inactive": true,
	}

	return CreateSource(client, sceneName, sourceName, sourceKind, settings)
}

func DeleteSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	sourceType string,
) error {
	// Id don't know what to put here???
	// I don't know what an 'item' map looks like??
	item := map[string]interface{}{
		"name":   sourceName,
		"type":   "input",
		"typeId": sourceType,
	}
	req := obsws.NewDeleteSceneItemRequest(
		sceneName, item, sourceName, 0,
	)
	_, err := client.execOBSCommand(&req)
	return err
}

// TODO: This should look to see if a source already exists
func CreateSource(
	client *OBSClient,
	sceneName string,
	sourceName string,
	sourceKind string,
	settings map[string]interface{},
) error {
	req := obsws.NewCreateSourceRequest(
		sourceName,
		sourceKind,
		sceneName,
		settings,
		true,
	)

	fmt.Printf("Creating Source: %+v\n", settings)

	// OBS has a different name for the location of the file
	// if its an image of a video source: thats for video
	var filename string
	if sourceKind == "ffmpeg_source" {
		filename = settings["local_file"].(string)
	} else {
		filename = settings["file"].(string)
	}
	width, height, memeType := memes.ExtractFileinfo(filename)

	ext := filepath.Ext(filename)
	newFilename := fmt.Sprintf("%s%s", sourceName, ext)
	m := memes.Meme{
		Name:         sourceName,
		X:            0.0,
		Y:            0.0,
		Scale:        1.0,
		Rotation:     0.0,
		PositionType: "default",
		Width:        width,
		Height:       height,
		MemeType:     memeType,
		Filename:     newFilename,
	}
	tx := client.DB.Create(&m)

	if tx.Error != nil {
		fmt.Printf("Error Creating Default Meme: %+v %+v\n", m, tx.Error)
	}

	_, err := client.execOBSCommand(&req)
	return err
}

// CreateFakeSourcer - Create a source with a timestamp
func CreateFakeSource(client *OBSClient) {
	rand.Seed(time.Now().UnixNano())
	fakeSource := fmt.Sprintf("fake_%s", time.Now())
	sourceKind := "ffmpeg_source"
	settings := map[string]interface{}{
		"local_file":          "",
		"looping":             true,
		"Render":              true,
		"close_when_inactive": true,
	}
	CreateSource(client, "fake", fakeSource, sourceKind, settings)
}
