package obs

import "gitlab.com/beginbot/beginsounds/pkg/memes"

// Should this also know it's Meme id
// if we have a source
// we can look up the current and default meme
// at the beginning
// then we can check later on if we have a current,
// to know to create or update

// Command - represents what a OBS command needs to be called
type Command struct {
	Scene       string
	Source      string
	Filters     []string
	Parts       []string
	DefaultMeme *memes.Meme
	CurrentMeme *memes.Meme
	Settings    map[string]interface{}
	Func        transformFunc
}

type transformFunc = func(
	*OBSClient,
	string, // Scene
	string, // Source
	map[string]interface{}, // Settings
) error

// FilterMap The set of filters the effect uses
// 		for every source, and value we have 1 channel
//    that blocks, so we only trigger effect at time
var FilterMap = map[string][]string{
	"!tall":      {"transform"},
	"!wide":      {"transform"},
	"!spin":      {"transform"},
	"!scale":     {"transform"},
	"!zoom":      {"transform"},
	"!slide":     {"position"},
	"!slideout":  {"position"},
	"!rotate":    {"rotate"},
	"!chad":      {"transform"},
	"!color":     {"color_correction"},
	"!peak":      {"position"},
	"!peek":      {"position"},
	"!zoomout":   {"transform"},
	"!move":      {"position"},
	"!fall":      {"position"},
	"!rise":      {"position"},
	"!unveil":    {"tranform"},
	"!show":      {"position"},
	"!hide":      {"position"},
	"!flash":     {"position"},
	"!default":   {"position"},
	"!moveup":    {"position"},
	"!movedown":  {"position"},
	"!moveright": {"position"},
	"!moveleft":  {"position"},
	// "!find": {"transform"},
	// "!hide": {"transform"},
	// "!zoom_out": {"transform"},
	// "!zs": {"transform"},
}

// CmdFuncMap what function to run for each command
var CmdFuncMap = map[string]transformFunc{
	"!wide":      WideSource,
	"!tall":      TallSource,
	"!moveup":    MoveSource,
	"!movedown":  MoveSource,
	"!moveright": MoveSource,
	"!moveleft":  MoveSource,
	"!spin":      Spin,
	"!scale":     ScaleSource,
	"!zoom":      Zoom,
	"!slide":     SlideSource,
	"!slideout":  SlideOutSource,
	"!peak":      SlideInFromSide,
	"!peek":      SlideInFromSide,
	"!rotate":    RotateSource,
	"!chad":      BigBrain,
	"!color":     FadeToColor,
	"!fall":      FallSource,
	"!rise":      RiseSource,
	"!zoomout":   ZoomOut,
	"!unveil":    UnveilSource,
	"!show":      ShowSource,
	"!hide":      HideSource,
	"!move":      MoveSource,
	"!flash":     FlashSource,
	"!default":   DefaultSource,
}
