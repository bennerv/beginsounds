package obs

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
	"gorm.io/gorm"
)

func visibleMemes(client *OBSClient, db *gorm.DB) ([]*memes.Meme, error) {
	ms := []*memes.Meme{}
	tx := db.Find(&ms)
	return ms, tx.Error

	// sources, _ := SourcesByVisibility(client, MemeScene)
	// for _, source := range sources {
	// 	meme, err := memes.Find(db, source)
	// 	if err != nil {
	// 		meme := MemeInfo(client, source)
	// 		meme.Save(db)
	// 		// meme, err := memes.Find(db, source)
	// 	}
	// 	ms = append(ms, meme)
	// }
	// return ms
}

func MemeSaver(
	client *OBSClient,
	db *gorm.DB,
	cmd string,
	obsCmd Command,
	results <-chan string,
) (bool, error) {

	effectTriggered := true
	parts := obsCmd.Parts

	switch cmd {
	case "!water_the_memes":
		ms, _ := visibleMemes(client, db)
		for _, m := range ms {
			m.PositionType = "current"
			m.Scale *= 2.0
			obsCmd.Settings["scale"] = m.Scale
			ScaleSource(client, MemeScene, m.Name, obsCmd.Settings)
			m.Save(db)
		}
	case "!honey_i_shrunk_the_memes":
		ms, _ := visibleMemes(client, db)
		for _, m := range ms {
			m.PositionType = "current"
			m.Scale /= 2.0

			obsCmd.Settings["scale"] = m.Scale
			ScaleSource(client, MemeScene, m.Name, obsCmd.Settings)
			m.Save(db)
		}

	case "!save_meme":
		meme := MemeInfo(client, obsCmd.Source)
		meme.PositionType = "current"
		err := meme.Save(db)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}
	case "!set_default", "!save_default":
		// The reason why we create a fake source,
		// is force OBS to trigger exporting the position
		// of all sources
		//
		// This is hack
		CreateFakeSource(client)
		meme := MemeInfo(client, obsCmd.Source)

		if obsCmd.DefaultMeme.ID == 0 {
			meme.PositionType = "default"
			fmt.Printf("Saving Settings %s %+v\n", obsCmd.Source, meme)

			err := meme.Save(db)
			if err != nil {
				fmt.Printf("err = %+v\n", err)
			}
		} else {
			tx := db.Model(&obsCmd.DefaultMeme).
				Where("ID = ?", obsCmd.DefaultMeme.ID).
				Updates(meme)

			if tx.Error != nil {
				fmt.Printf("err = %+v\n", tx.Error)
			}
		}
	case "!restore_scene":
		var name string
		if len(parts) > 2 {
			name = "test1"
		} else {
			name = parts[1]
		}
		var ms []memes.Meme
		memeFile := fmt.Sprintf("assets/ViewerSceneJson/%s.json", name)
		data, err := ioutil.ReadFile(memeFile)
		if err != nil {
			errMsg := fmt.Sprintf(
				fmt.Sprintf("%v Error Reading in %s", err, memeFile),
			)
			return false, errors.New(errMsg)
		}
		err = json.Unmarshal(data, &ms)

		for i, meme := range ms {
			xModifier := 10.0
			settings := map[string]interface{}{
				"modifier": xModifier,
				"start_x":  0.0,
				"end_x":    meme.X,
				"start_y":  -200.0,
				"end_y":    meme.Y,
				"y":        meme.Y,
				"x":        meme.X,
				"scale":    meme.Scale,
				"rotation": meme.Rotation,
			}

			if i%2 == 0 {
				go SlideSource(client, MemeScene, meme.Name, settings)
			} else {
				go FallSource(client, MemeScene, meme.Name, settings)
			}

			err := meme.Save(db)
			if err != nil {
				fmt.Printf("err Saving Meme %+v\n", err)
			}

		}

	case "!save_scene":
		fmt.Println("Attempting to Save Scene")
		ms := []memes.Meme{}
		// This saves the position of all
		// sources thought a goddamn hack
		var name string
		if len(parts) < 2 {
			name = "begintest"
		} else {
			name = parts[1]
		}

		CreateFakeSource(client)
		sources, _ := SourcesByVisibility(client, MemeScene)
		for _, source := range sources {
			meme := MemeInfo(client, source)
			ms = append(ms, meme)
		}
		memeFile, err := os.Create(fmt.Sprintf("assets/ViewerSceneJson/%s.json", name))
		if err != nil {
			fmt.Printf("\nError Creating Built JSON: %s", err)
		}
		encoder := json.NewEncoder(memeFile)
		encoder.Encode(ms)
		memeFile.Close()
	default:
		effectTriggered = false
	}

	return effectTriggered, nil
}
