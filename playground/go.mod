module gitlab.com/beginbot/playground

go 1.15

replace gitlab.com/beginbot/beginsounds => ../../beginsounds

require (
	github.com/davidbegin/go-obs-websocket v0.0.0-20201228163259-e7dae0cf1ca2
	gitlab.com/beginbot/beginsounds v0.0.0-20201213204443-159c57336316
)
