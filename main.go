package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/beginbot/beginsounds/cmd/obs"
	"gitlab.com/beginbot/beginsounds/notifs"
	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/chat_saver"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme_router"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/criminal_activities"
	"gitlab.com/beginbot/beginsounds/pkg/cube_bet_router"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/economy_router"
	"gitlab.com/beginbot/beginsounds/pkg/filter"
	"gitlab.com/beginbot/beginsounds/pkg/hand_of_the_market"
	"gitlab.com/beginbot/beginsounds/pkg/info_router"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/media_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/media_router"
	"gitlab.com/beginbot/beginsounds/pkg/notification_site"
	"gitlab.com/beginbot/beginsounds/pkg/relationship_manager"
	"gitlab.com/beginbot/beginsounds/pkg/reporter"
	"gitlab.com/beginbot/beginsounds/pkg/router"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_processor"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request_router"
	"gitlab.com/beginbot/beginsounds/pkg/streamgod_router"
	"gitlab.com/beginbot/beginsounds/pkg/tee"
	"gitlab.com/beginbot/beginsounds/pkg/utils"
)

type ResponseChannels struct {
	beginChat              chan string
	beginbotbotChat        chan string
	websocketNotifications chan string
	audioRequests          chan audio_request.AudioRequest
	obsCommands            chan chat.ChatMessage
}

// TODO: All of these router functions should share an type interface
func main() {
	db := database.CreateDBConn("beginsounds4")
	// How do we clone the database easily?? pgdumb and pgrestore

	twitchChannel := flag.String("channel", "beginbot", "The main IRC Channel to connect to")
	isMarketOpen := flag.Bool("market", false, "If the market is open")
	connectToOBS := flag.Bool("obs", false, "Whether to connect to OBS")
	enableWebsocketNotifs := flag.Bool("websocket",
		false,
		"Whether to Send Websocket Notifications",
	)
	wsListenAddr := flag.String("wsaddr", ":8080", "Address of the websocket listener.")

	flag.Parse()

	ctx := context.Background()
	themes := colorscheme.GatherColors()

	fmt.Printf("Main Bot Running on Twitch Channel: %#v\n", *twitchChannel)
	c := config.NewIrcConfig(*twitchChannel)

	// We will need to launch another Go Routine to read
	// off from beginbotbot channel
	// and only respond to messages from Streamlords and Streamgods
	botChannelConfig := config.NewIrcConfig("beginbotbot")
	backAlleyMsgs := irc.ReadIrc(ctx, botChannelConfig.Conn)
	backAlleyCmds := router.FilterChatMsgs(ctx, db, &botChannelConfig, backAlleyMsgs)

	// Launch a Go Routine to collect messages from IRC
	messages := irc.ReadIrc(ctx, c.Conn)

	// Filter out only the User Chat messages from IRC
	// chatMsgs := router.FilterChatMsgs(ctx, db, &botChannelConfig, messages)
	beginbotMsgs := router.FilterChatMsgs(ctx, db, &c, messages)

	chatMsgs := tee.ChatFanIn(beginbotMsgs, backAlleyCmds)

	// Duplicate the User Messages, one to save one for further processing
	//  Fan In here the beginbot and beginbotbot messages
	chatMsgs1, chatMsgs2 := tee.DuplicateChatMessages(ctx, chatMsgs)

	// Why am I not using make
	var botResponses = []<-chan string{}
	var botbotResponses = []<-chan string{}
	var audioRequests = []<-chan audio_request.AudioRequest{}

	// Save the User Chat Messages
	// If the User hasn't chatted today, then a Theme Request
	// Will also be played
	//
	// So how should we add OBS themes on theme requests
	// can we return them here
	themeRequests, obsThemes := chat_saver.SaveChat(ctx, db, chatMsgs1)
	audioRequests = append(audioRequests, themeRequests)

	// This filters out just attempted user commands
	userCommands, botResponses1 := filter.RouteUserCommands(ctx, db, chatMsgs2)
	botResponses = append(botResponses, botResponses1)

	ircChans := tee.FanOut2BackInTheHabit(
		ctx,
		userCommands,
	)

	// !listpacks
	// !peakpack
	// !createpack
	// !addtopack
	// !removefrompack
	// !droppack
	// !buypack
	uc, ircChans := ircChans[0], ircChans[1:]
	adminResults2, botResponses18 := economy_router.PackRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses18)
	botbotResponses = append(botbotResponses, adminResults2)

	uc, ircChans = ircChans[0], ircChans[1:]
	adminResults1, chatResults, obsRequests3 := cube_bet_router.Route(ctx, db, uc)
	botResponses = append(botResponses, chatResults)
	botbotResponses = append(botbotResponses, adminResults1)

	// NoMemes Channels
	//
	// !passthejester
	// !unapproveparty
	// !approveparty
	// !unapprovedparties
	// !nomeme
	// !artleaker
	// !chaos
	// !dropeffect
	// !cage
	// !release
	// !enable
	// !disable
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses1,
		websocketResponses,
		botResponses17,
		audioRequests5,
		noMemes,
		obsRequests4 := streamgod_router.Route(
		ctx,
		db,
		chat.DBChan{DB: db, Messages: uc},
	)
	botResponses = append(botResponses, botResponses17)
	botbotResponses = append(botbotResponses, botbotResponses1)
	audioRequests = append(audioRequests, audioRequests5)

	// !pokemon
	// TODO: Refactor this
	uc, ircChans = ircChans[0], ircChans[1:]
	ars1, botResponses11 := filter.RoutePokemonCommands(ctx, db, uc)
	audioRequests = append(audioRequests, ars1)
	botResponses = append(botResponses, botResponses11)

	// Process audio requests and saves them in the DB to Played later
	uc, ircChans = ircChans[0], ircChans[1:]
	audioRequests3, botResponses10, obsRequests := filter.AudioRequests(ctx, db, uc)
	botResponses = append(botResponses, botResponses10)
	audioRequests = append(audioRequests, audioRequests3)

	// !help
	// !wtf
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses21 := info_router.Route(ctx, db, uc)
	botResponses = append(botResponses, botResponses21)

	// !color
	// !colors
	// !setcolor
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses3 := colorscheme_router.Route(ctx, db, uc, themes)
	botResponses = append(botResponses, botResponses3)

	// !props
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses4 := economy_router.PropsRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses4)

	// !perms
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses14 := economy_router.PermsRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses14)

	// !buy
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses13 := economy_router.BuyRoute(ctx, db, uc)
	botResponses = append(botResponses, botResponses13)

	// !give
	// !donate
	// !clone -> the word clone is also used by OBS code
	// !duplicate
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses5, botResponses22 := economy_router.GivingRouter(ctx, db, uc)
	botResponses = append(botResponses, botResponses22)
	botbotResponses = append(botbotResponses, botbotResponses5)

	// !me
	// !jester
	// !formparty
	// !parties
	// !join
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses9 := economy_router.MeRoute(ctx, db, uc)
	botResponses = append(botResponses, botResponses9)

	// !image
	// !video
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses2, botResponses19 := media_router.Route(ctx, db, uc)
	botResponses = append(botResponses, botResponses19)

	botbotResponses3, botResponses20, obsResponses1 := media_request_processor.Process(ctx, db)
	botbotResponses = append(botbotResponses, botbotResponses3)
	botResponses = append(botResponses, botResponses20)

	// !soundeffect
	// !requests
	// !approve
	// !deny
	uc, ircChans = ircChans[0], ircChans[1:]
	botbotResponses2, botResponses5 := soundeffect_request_router.Route(ctx, db, uc)
	botResponses = append(botResponses, botResponses5)
	botbotResponses = append(botbotResponses, botbotResponses2)

	// !love
	// !hate
	// !props
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses8 := relationship_manager.Manage(ctx, db, uc)
	botResponses = append(botResponses, botResponses8)

	// This runs every second to process any new soundeffect requests
	botResponses6, audioRequests2 := soundeffect_request_processor.Process(ctx, db)
	botResponses = append(botResponses, botResponses6)
	audioRequests = append(audioRequests, audioRequests2)

	// Drops Mana and Street Cred
	// Also Leaks credentials ssshhh
	if *isMarketOpen {
		botResponses7 := hand_of_the_market.Serve(ctx, db)
		botResponses = append(botResponses, botResponses7)
	}

	// !steal
	uc, ircChans = ircChans[0], ircChans[1:]
	botResponses12 := criminal_activities.Serve(ctx, chat.DBChan{DB: db, Messages: uc})
	botResponses = append(botResponses, botResponses12)

	// Fan-In the User Soundeffect Requests and the Automatic Theme songs
	// Then pass them to the soundboard to execute them
	playSoundRequests := utils.AudioRequestFanIn(ctx, audioRequests...)

	// At this point, we need to pass in a No-Meme Channels
	// This is what actually plays the sounds
	botResponses2, notifications1, obsRequests2 := soundboard.Execute(
		ctx,
		db,
		playSoundRequests,
		noMemes,
	)
	botResponses = append(botResponses, botResponses2)

	if *connectToOBS {
		uc, ircChans = ircChans[0], ircChans[1:]
		obsCommands := tee.ChatFanIn(
			uc,
			obsThemes,
			obsRequests,
			obsRequests2,
			obsResponses1,
			obsRequests3,
			obsRequests4,
		)
		botResponses16 := obs.TrollBegin(db, obsCommands)
		botResponses = append(botResponses, botResponses16)
	}

	// Fan in all the Bot Responses before Reporting
	allResults := utils.StringFanIn(ctx, botResponses...)

	// r1, r2 := tee.FanOutString(allResults)
	// Ahh the other type of message ahhhhhhhhh
	if *enableWebsocketNotifs {
		go notifs.LaunchNotifications(*wsListenAddr, websocketResponses)
	}

	// Pass some messages in here
	// Also send another internal channel
	// We some type of request

	// TODO: Add flag to turn this off
	// Send beginbotbot responses back to Twitch Chat
	// reporter.Report(ctx, allResults, &c)
	reporter.PrimaryReport(ctx, allResults, &c, &botChannelConfig)

	// Fan
	allBotBotResponses := utils.StringFanIn(ctx, botbotResponses...)
	reporter.Report(ctx, allBotBotResponses, &botChannelConfig)
	// reporter.Report(ctx, allResults, &botChannelConfig)

	// So we should only take in notifications here
	go notification_site.Serve(ctx, notifications1)

	for {
		select {
		case <-ctx.Done():
		default:
		}
	}
}
