# HELP / FAQ

## Basic Commands

!me
!props
!buy

## Stream Jester

What is the Stream Jester?

How do I come the Stream Jester?

What can I do as Jester?

## Playing Sounds

- Streamgods can play all sounds, without costing Mana, or affecting the price
  of soundeffects

- Stream Jesters can play all sounds, but it costs them 1 Mana.

- All other users have to own the sound, and they can then play the sound for 1
  Mana.

## Adding Images to the Source

- If you are a Streamlord or get approval from a stream lord, you can add
  images, gifs and videos to the stream! Then these sources can manipulated
  with various effects in the chat.

!move obieru 300 300
!show obieru
!bigbrain obieru
!chad obieru
!scale obieru 0.3
!random obieru
!addfilters obieru
!rotate obieru 0

```
!image URL NAME
!gif   URL NAME
!video URL NAME
```

Ways to manipulate the sources:

```
// We need to be able to see Begin code sometime!
!hide NAME

// Show the source
!show NAME

// 1 is fullsize 0.5 would be Half
!scale NAME 0.5

// This is an absolute x and y on the OBS Scene
!move NAME X Y
!move NAME 300 300

// This is absolute rotation 0 is return to no rotation
!rotate NAME 90

// This triggers a random effect on the source
!random NAME

// These are specific effects on the created source
!slide NAME
!bigbrain NAME
!chad NAME
```

## Love / Hating others

You can't split your streetcred amoungs random people.
We are trying to prevent, people from just throwing money in the chat.
Our goal is encourage people to find the other chatters they vibe with.

We introduce constraints to the number of you can love, and who
you can easily split your street cred amoung, to enforce these
spheres of influence.

- !love / !hate
  - You can love +1 person per how many love you.
  - +1 for each lover for your street cred
  - default is +3
- !lovers
  - Seeing who you love, who loves you

## Commands

- !buy
  - !buy
  - !buy 20  -> the max to show all the names in Twitch
  - !buy 100 -> more than 20 it shows the amount and Cool Points
- !perms
  - !perms damn
  - !perms zanuss
- !props
  - !props
  - !props 10
  - !props @zanuss
- !soundeffect
  -> should be working now, and better
- !me
  - if you want your page refreshed !me
  - if you get access denied, wait a second and refresh (!me triggers generation
    and upload of the page)
- !color
- !pokemon
- !guess
- !props

## Demo

!rise
!normie_transform
!zoom2
!pulse

## Packs

Packs are collections of sounds, you can buy.

Examples:

```
!listpacks

!buypack prime

!peakpack prime
```

### Stream Lord Only Commands

!createpack
!addtopack
!removefrompack
!droppack
