package main

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestBasicEconomyFeatures(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	username := "beginbot"
	command := "damn"

	p := player.CreatePlayerFromName(db, username)
	c := stream_command.CreateFromName(db, command)

	if permissions.IsAllowed(db, p.ID, c.ID) {
		t.Fatalf("%s Should NOT be allowed to play !%s", username, command)
	}

	permissions.Allow(db, p.ID, c.ID)

	if !permissions.IsAllowed(db, p.ID, c.ID) {
		t.Fatalf("For some reason %s is NOT allowed to play !%s",
			username, command)
	}

	// audioRequest := audio_request.AudioRequest{Name: }
	// audioRequest, _ := soundboard.CreateAudioRequest(db, command, username)
	// fmt.Printf("audioRequest = %+v\n", audioRequest)
	// // How do we get this not to play in Tests
	// timer := time.NewTimer(1 * time.Millisecond)
	// played := soundboard.PlayAudio(*audioRequest, timer)
	// fmt.Printf("played = %+v\n", played)

	// PlayAudio(audioRequest audio_request.AudioRequest) bool {

	// Play

	// Make sure Price Decayed

	// Buy

	// Check Account
}
