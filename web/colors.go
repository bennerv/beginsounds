package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/beginbot/beginsounds/pkg/colorscheme"
)

type ThemePage struct {
	VotesByUser []colorscheme.VoteCount
	Domain      string
	Theme       string
	Count       int64
	Color0      string
	Color1      string
	Color2      string
	Color3      string
	Color4      string
	Color5      string
	Color6      string
	Color7      string
	Color8      string
	Color9      string
	Color10     string
	Color11     string
	Color12     string
	Color13     string
	Color14     string
	Color15     string
	Metadata
}

func ThemeHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	theme := vars["theme"]
	count := colorscheme.NewCountByTheme(db, theme)

	cs := colorscheme.ParseColorScheme(theme)
	votesByUser := colorscheme.VotesByUser(db, theme)

	page := ThemePage{
		VotesByUser: votesByUser,
		Metadata:    metadata,
		Domain:      "http://localhost:1992",
		Theme:       theme,
		Count:       count,
		Color0:      cs.Colors.Color0,
		Color1:      cs.Colors.Color1,
		Color2:      cs.Colors.Color2,
		Color3:      cs.Colors.Color3,
		Color4:      cs.Colors.Color4,
		Color5:      cs.Colors.Color5,
		Color6:      cs.Colors.Color6,
		Color7:      cs.Colors.Color7,
		Color8:      cs.Colors.Color8,
		Color9:      cs.Colors.Color9,
		Color10:     cs.Colors.Color10,
		Color11:     cs.Colors.Color11,
		Color12:     cs.Colors.Color12,
		Color13:     cs.Colors.Color13,
		Color14:     cs.Colors.Color14,
		Color15:     cs.Colors.Color15}

	themeTmpl.Execute(w, page)
}

func ColorsHandler(w http.ResponseWriter, r *http.Request) {
	totalVoteCount := colorscheme.Count(db)
	themesByCount := colorscheme.ThemesByCount(db)

	w.WriteHeader(http.StatusOK)

	page := HomePage{
		TotalCount:    totalVoteCount,
		Metadata:      metadata,
		ThemesByCount: themesByCount}

	colorsTmpl.Execute(w, page)
}
