package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gorm.io/gorm"
)

type PacksPage struct {
	// Metadata
	Domain    string
	Extension string
	Packs     []pack.PackResult
}

func GeneratePacks(db1 *gorm.DB) {
	ps, err := pack.All(db1)
	if err != nil {
		fmt.Printf("Error Fetching All Packs: %+v\n", err)
	}

	remotePage := PacksPage{
		// Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Domain:    "http://beginworld.exchange",
		Extension: ".html",
		Packs:     ps,
	}
	buildFile := fmt.Sprintf("build/packs.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = packsTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	// We should sync here as well
}

func PacksHandler(w http.ResponseWriter, r *http.Request) {
	ps, err := pack.All(db)
	fmt.Printf("Players = %+v\n", ps[0].Players)
	fmt.Printf("len players = %+v\n", len(ps[0].Players))

	if err != nil {
		fmt.Printf("Error Fetching All Packs: %+v\n", err)
	}

	page := PacksPage{
		// Metadata: localMetadata,
		Domain: "http://localhost:1992",
		Packs:  ps,
	}
	packsTmpl.Execute(w, page)
}
