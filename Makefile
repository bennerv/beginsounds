default:
	go build -gcflags="-e" -o bin/ .

.PHONY: web
web:
	go build -o bin/ -gcflags="-e" web/app.go

.PHONY: test
test:
	grc gotest ./...

.PHONY: migrations
migrations:
	mkdir -p bin
	go build -o bin/ cmd/migrations/migrations.go
	./bin/migrations -db test_beginsounds3 init
	./bin/migrations -db test_beginsounds3

ci_test:
	go test ./...

t:
	GOTRACEBACK=all gotest -race ./... -v

fmt:
	go fmt ./...

test_leak:
	s3cmd get --recursive                                   \
		--access_key=73MQWUEDU7RI00UVOF21 \
		--secret_key=eWYs0AD3sjVD56sDCgN2RSP91ozQftFEVCvwb6Lv \
	 	s3://scavenger-bucket/

sync:
	s3cmd --no-mime-magic --acl-public put build/index.html s3://beginworld/index.html
	s3cmd --no-mime-magic --acl-public put build/index.html s3://beginworld/404.html
	s3cmd --no-mime-magic --acl-public put build/help.html s3://beginworld/help.html

	s3cmd --no-mime-magic --acl-public put build/user_requests.html s3://beginworld/user_requests.html
	s3cmd --no-mime-magic --acl-public put build/parties.html s3://beginworld/parties.html
	s3cmd --no-mime-magic --acl-public put build/memes.html s3://beginworld/memes.html

	s3cmd --no-mime-magic --acl-public put build/commands.json s3://beginworld/commands.json
	s3cmd --no-mime-magic --acl-public put build/memes.json s3://beginworld/memes.json
	s3cmd --no-mime-magic --acl-public put build/parties.json s3://beginworld/parties.json

	# s3cmd --no-mime-magic --acl-public put build/commands.html s3://beginworld/commands.html
	# s3cmd --no-mime-magic --acl-public put build/users.html s3://beginworld/users.html
	# s3cmd --no-mime-magic --delete-after put build/index.html s3://beginworld/index.html

sync_all:
	rm -rf upload/*
	mv -f build/* upload
	mkdir build/users build/commands
	s3cmd --no-mime-magic --acl-public --delete-after --recursive put upload/ s3://beginworld/

commands:
	s3cmd --no-mime-magic --acl-public --delete-after --recursive put build/commands/ s3://beginworld/commands/
	# s3cmd --delete-after sync build/commands/ s3://beginworld/commands/


unapproved_memes:
	s3cmd --no-mime-magic --acl-public --delete-after --recursive put \
		/home/begin/code/beginsounds/tmp/memes \
		s3://beginworld/memes/
	# s3cmd --no-mime-magic --acl-public --delete-after --recursive put \
	# 	/home/begin/stream/Stream/ViewerVideos \
	# 	s3://beginworld/memes/

memes:
	s3cmd --no-mime-magic --acl-public --recursive put \
		/home/begin/stream/Stream/ViewerImages/ \
		s3://beginworld/memes/
	s3cmd --no-mime-magic --acl-public --recursive put \
		/home/begin/stream/Stream/ViewerVideos/ \
		s3://beginworld/memes/

css:
	s3cmd --delete-after sync ../chat_thief/build/beginworld_finance/styles/ s3://beginworld/styles/
	# s3cmd --no-mime-magic --acl-public --delete-after put ../chat_thief/build/beginworld_finance/styles/beginbot.css s3://beginworld/styles/beginbot.css
	# s3cmd --no-mime-magic --acl-public --delete-after sync build/ s3://beginworld/

sounds:
	s3cmd put -r --exclude "*" \
		--no-mime-magic --acl-public \
		--include "*.mp3"       \
		--include "*.wav"       \
		--include "*.m4a"       \
		--include "*.opus"      \
		--include "*.ogg"      \
		"/home/begin/stream/Stream/Samples/" s3://beginworld/media/
	s3cmd put -r --exclude "*" \
		--no-mime-magic --acl-public \
		--include "*.mp3"       \
		--include "*.wav"       \
		--include "*.m4a"       \
		--include "*.opus"      \
		--include "*.ogg"      \
		"/home/begin/stream/Stream/Samples/theme_songs/" s3://beginworld/media/

app:
	rm bin/web; cd web; go build -o ../bin/ .; cd ..; bin/web

ls_memes:
	s3cmd ls s3://beginworld/memes/ | tee memes1.txt
	# s3cmd ls s3://beginworld/memes/memes/ | tee memes_memes.txt
	# s3cmd ls s3://beginworld/memes/ViewerImages/ | tee viewer_images.txt
	# s3cmd ls s3://beginworld/memes/ViewerVideos/ | tee viewer_videos.txt
